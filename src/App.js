import './App.scss';
import './assets/wowo.css';
import React, {useEffect, useState} from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import PageLoadingComponent from "./components/page-loading/page-loading";
import Header from "./layouts/header";
import Footer from "./layouts/footer";
import PageNotFound from "./components/page-error/page-not-found";
import Sidebar from "./layouts/sidebar";
import "antd/dist/antd.css";
import AuthService from "./services/auth.service";

function App() {

    const [userLoading, setUserLoading] = useState(true);
    const [userObject, setUserObject] = useState(undefined);

    useEffect(() => {
        const accessToken = AuthService.getToken('access');
        if (accessToken) {
            AuthService.getCurrentUser().then(
                res => {
                    setUserLoading(false);
                    if (res.status === 200) {
                        setUserObject(res.data);
                    } else {
                        setUserObject(undefined);
                    }
                }
            );
        } else {
            setUserLoading(false);
        }
    }, []);

    return !userLoading ? (
        <React.Suspense fallback={<PageLoadingComponent/>}>
            <BrowserRouter>
                <Header userObject={userObject} />
                <Switch>
                    <Route path={'/'} component={React.lazy(() => import('./layouts/default-layout'))} />
                    <Route render={() => <PageNotFound />} />
                </Switch>
                <Sidebar userObject={userObject} />
                <div id={'modals'} />
                <Footer userObject={userObject} />
            </BrowserRouter>
        </React.Suspense>
    ): (<PageLoadingComponent />);
}

export default App;
