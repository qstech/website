import React, {useState, useEffect} from "react";
import {Helmet} from "react-helmet";
import PagePaddingComponent from "../../page-padding/page-padding";
import {withRouter} from "react-router";
import './account-center.scss';
import {Collapse, Form, Table} from "antd";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronUp} from "@fortawesome/free-solid-svg-icons";
import countries from '../../../assets/counties.json';
import AuthService from "../../../services/auth.service";
import {useForm} from "antd/es/form/Form";
import PageLoadingComponent from "../../page-loading/page-loading";
import axiosService from "../../../services/axios.service";
import moment from 'moment'

const AccountCenter = () => {
    const [tableLoading, setTableLoading] = useState(false);
    const [clientList, setClientList] = useState();
    const [addEmail, setAddEmail] = useState('');
    const [isAgent, setIsAgent] = useState(false);
    const [userObject, setUserObject] = useState(null);
    const [collapseOpen, setCollapseOpen] = useState(true);
    const [form] = useForm();

    useEffect(() => {
        setTableLoading(true);
        AuthService.getCurrentUser().then(
            res => {
                setTableLoading(false);
                setClientList(res.data?.clients);
                setIsAgent(res.data?.user_type === 'Agent')
                setUserObject(res.data)
                form.setFieldsValue(res.data)
            }
        ).catch();
        const emptyValues = Object.values(form.getFieldsValue()).filter(i => i !== '')
        if (emptyValues?.length > 0) {
            setCollapseOpen(true);
        } else {
            setCollapseOpen(false);
        }
    }, [form])

    const FormLabel = ({text}) => (
        <span className='form-label'>{text}</span>
    )

    const submitForm = (values) => {
        axiosService().post('users/update/', values).then(
            () => window.location.reload()
        )
    }

    return (
        tableLoading ?
            <PageLoadingComponent />
            :
        <div className='account-center-container'>
            <Helmet><title>{isAgent ? 'Agent centre' : 'Account Center'}</title></Helmet>
            <div className={'wrap fixed-width-container'}>
                <PagePaddingComponent />
                <div className="title wowo fadeInUp animated">
                    <div className="left">
                        <h1>{isAgent ? 'Agent centre' : 'Account Center'}</h1>
                    </div>
                    <div className="right">
                        {userObject?.first_name ? <span>Welcome back {userObject?.first_name}!</span> : undefined}
                    </div>
                </div>
                <div className='content-section wowo fadeInUp animated'>
                    <div className='left-section'>
                        {
                            userObject?.first_name ?
                                (!isAgent ? <h3>Member area coming soon</h3>
                                    :
                                    <>
                                        <h3>My Clients</h3>
                                        <Table
                                            pagination={false}
                                            dataSource={clientList}
                                            loading={tableLoading}
                                            rowKey='name'
                                            width='100%'
                                            columns={[
                                                {
                                                    dataIndex: 'name',
                                                    title: 'Name',
                                                    sorter: true,
                                                    width: '20%'
                                                },
                                                {
                                                    dataIndex: 'email',
                                                    title: 'Email',
                                                    width: '30%'
                                                },
                                                {
                                                    dataIndex: 'created',
                                                    title: 'Registered On',
                                                    sorter: true,
                                                    width: '25%',
                                                    render: (text) => (
                                                        moment(text).format('YYYY/MM/DD')
                                                    )
                                                },
                                                {
                                                    dataIndex: 'last_login',
                                                    title: 'Last Login',
                                                    sorter: true,
                                                    width: '25%',
                                                    render: (text) => (
                                                        moment(text).format('YYYY/MM/DD')
                                                    )
                                                }
                                            ]}
                                        />
                                    </>)
                                :
                                <h3>You are not logged in yet</h3>
                        }
                    </div>
                    {
                        userObject?.first_name ? (
                            <div className='right-section'>
                                {
                                    isAgent && (
                                        <>
                                            <h3>Add clients to your list</h3>
                                            <p>
                                                Sign up for a customer using the form below
                                                an they will receive an email with a link to set their password
                                                and log in.
                                            </p>
                                            <div className='invite-container'>
                                                <input
                                                    value={addEmail}
                                                    type='email'
                                                    placeholder='Client email'
                                                    onChange={
                                                        (e) => {
                                                            setAddEmail(e.target.value)
                                                        }}
                                                />
                                                <a>Invite</a>
                                            </div>
                                        </>
                                    )
                                }
                                <Collapse
                                    className='info'
                                    ghost
                                    expandIconPosition='right'
                                    expandIcon={(props) => {
                                        return (
                                            <FontAwesomeIcon
                                                icon={faChevronUp}
                                                className={
                                                    `collapse-icon ${props.isActive ? 'is-active' : ''}`
                                                }
                                            />
                                        );
                                    }}
                                    defaultActiveKey={collapseOpen ? 1 : null}
                                >
                                    <Collapse.Panel
                                        key={1}
                                        header={<h3>Update your details</h3>}
                                    >
                                        <Form layout='vertical' onFinish={submitForm} form={form}>
                                            <Form.Item
                                                name={'first_name'}
                                                required
                                                label={<FormLabel text='First Name' />}
                                            >
                                                <input />
                                            </Form.Item>
                                            <Form.Item
                                                name={'last_name'}
                                                required
                                                label={<FormLabel text='Last Name' />}
                                            >
                                                <input />
                                            </Form.Item>
                                            <Form.Item
                                                name={'email'}
                                                label={<FormLabel text='Email' />}
                                            >
                                                <input disabled />
                                            </Form.Item>
                                            <Form.Item
                                                name={'phone'}
                                                required
                                                label={<FormLabel text='Phone' />}
                                            >
                                                <input />
                                            </Form.Item>
                                            <Form.Item
                                                name={'wechat'}
                                                label={<FormLabel text='WeChat' />}
                                            >
                                                <input />
                                            </Form.Item>
                                            <Form.Item
                                                name={'whatsapp'}
                                                label={<FormLabel text='WhatsApp' />}
                                            >
                                                <input />
                                            </Form.Item>
                                            <Form.Item
                                                name={'language'}
                                                label={<FormLabel text='Preferred Language' />}
                                            >
                                                <select placeholder='Please select your language preference'>
                                                    <option value='english'>English</option>
                                                    <option value='chinese'>中文</option>
                                                </select>
                                            </Form.Item>
                                            <Form.Item
                                                required
                                                name={'country'}
                                                label={<FormLabel text='Country of Residence' />}
                                            >
                                                <select>
                                                    <option value=''>Please select your country of residence</option>
                                                    {
                                                        countries?.map(
                                                            item => (
                                                                <option
                                                                    key={item.code}
                                                                    value={item.name}
                                                                >
                                                                    {item.name}
                                                                </option>
                                                            )
                                                        )
                                                    }
                                                </select>
                                            </Form.Item>
                                            <Form.Item
                                                name={'interest'}
                                                label={<FormLabel text='Interest' />}
                                            >
                                                <select>
                                                    <option value=''>Please select your area of interest</option>
                                                    <option value="Investment">Investment</option>
                                                    <option value="Owner Occupy">Owner Occupy</option>
                                                    <option value="Monitoring the market">Monitoring the market</option>
                                                    <option value="Other">Other</option>
                                                </select>
                                            </Form.Item>
                                            <Form.Item
                                                name={'source'}
                                                label={<FormLabel text='How did you hear about us' />}
                                            >
                                                <select>
                                                    <option value=''>Please select how you heard about us</option>
                                                    <option value="Agent">Agent</option>
                                                    <option value="Word of Mouth">Word of Mouth</option>
                                                    <option value="Google">Google</option>
                                                    <option value="Online Ads">Online Ads</option>
                                                    <option value="Social Media">Social Media</option>
                                                    <option value="Other">Other</option>
                                                </select>
                                            </Form.Item>
                                            <Form.Item className='submit'>
                                                <input type='submit' value='Update' />
                                            </Form.Item>
                                        </Form>
                                    </Collapse.Panel>
                                </Collapse>
                            </div>
                        ): undefined
                    }
                </div>
            </div>
        </div>
    )
}

export default withRouter(AccountCenter);
