import {withRouter} from "react-router";
import React, {Component} from "react";
import axiosService from "../../../services/axios.service";
import PageNotFound from "../../page-error/page-not-found";
import ArticleDetailComponent from "../common/article-details/article-details";
import './detail-view.scss';

class MarketInsightDetailView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            articleObject: undefined,
            pageNotFound: false
        }
    }

    componentDidMount() {
        const slug = this.props.match.params.slug;
        axiosService().get(
            `articles/market-insight/${slug}/`
        ).then(
            res => {
                if (res.status === 200) {
                    this.setState({
                        ...this.state,
                        articleObject: res.data,
                        pageNotFound: false
                    })
                } else {
                    this.setState({
                        articleObject: undefined,
                        pageNotFound: true
                    })
                }
            }
        );
    }

    render() {
        return this.state.pageNotFound ? <PageNotFound /> : <ArticleDetailComponent articleObject={this.state.articleObject} objectType={'market-insight'}/>;
    }
}

export default withRouter(MarketInsightDetailView);
