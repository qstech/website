import PageLoadingComponent from "../../page-loading/page-loading";
import {Helmet} from "react-helmet";
import {Route, Switch, withRouter} from "react-router";
import PageNotFound from "../../page-error/page-not-found";
import React, {Component} from "react";

class MarketInsightComponent extends Component {
    render() {
        return (
            <React.Suspense fallback={<PageLoadingComponent />}>
                <Helmet><title>Market Insight</title></Helmet>
                <Switch>
                    <Route exact path={'/market-insight/'} component={React.lazy(() => import('./list-view'))} />
                    <Route exact path={'/market-insight/:slug'} component={React.lazy(() => import('./detail-view'))} />
                    <Route render={()=> <PageNotFound />} />
                </Switch>
            </React.Suspense>
        );
    }
}

export default withRouter(MarketInsightComponent);

