import {withRouter} from "react-router";
import React, {Component} from "react";
import {Link} from "react-router-dom";
import PagePaddingComponent from "../../page-padding/page-padding";
import "./list-view.scss";
import axiosService from "../../../services/axios.service";
import {Card} from "antd";
import Utils from "../../../layouts/utils";
import PageFooterComponent from "../../page-footer/page-footer";

class MarketInsightListView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: undefined,
            notFound: false,
            page: 1,
            tag: undefined
        }
    }

    componentDidMount() {
        const urlParams = new URLSearchParams(this.props.location.search);
        const tag = urlParams.get('tag');
        if (tag) {
            this.setState({
                tag: tag
            })
        }
        axiosService().post('articles/',
            {
                page: this.state.page,
                items_per_page: 6,
                type: 'market-insight',
                tag: tag || null
            }
        ).then(
            res => {
                if (res.status === 200) {
                    this.setState({
                        items: res.data
                    });
                } else {
                    this.setState({
                        notFound: true
                    });
                }
            }
        );
    }

    refreshList = (pageNumber) => {
        axiosService().post(`articles/`, {
            page: pageNumber,
            items_per_page: 6,
            type: 'market-insight',
            tag: this.state.tag || null
        }).then(
            res => {
                if (res.status === 200) {
                    this.setState({
                        items: res.data,
                        page: pageNumber
                    });
                } else {
                    this.setState({
                        notFound: true,
                        page: 1
                    });
                }
            }
        );
    }

    render() {
        return (
            <div className={'market-insight-list-page'}>
                <PagePaddingComponent />
                <div className={'wrap fixed-width-container'}>
                    <div className="title wowo fadeInUp animated">
                        <div className="left">
                            <h1>Market Insight</h1>
                        </div>
                        <div className="right">
                            <Link to={"/market-insight/?tag=expert-advice"}>Expert Advice<span className={'arrow-right'} /></Link>
                            <Link to={"/market-insight/?tag=investor-buyers-guide"}>Investor & Buyers Guide<span className={'arrow-right'} /></Link>
                        </div>
                        {
                            this.state.tag ?
                                <div className="tag-name">{ this.state.items?.tag }</div> :
                                undefined
                        }

                    </div>
                    <div className="contact-section wowo fadeInUp animated">
                        <p className="text">
                            Do you need more detailed advice? Leave us a message and one of our agents will answer it.</p>
                        <br />
                        <div>
                            <Link to={'/contact-us'}>Contact Agent</Link>
                        </div>
                    </div>
                    <div className={'list'}>
                        {
                            this.state.notFound ? <h2 className="no-data wowo animated fadeInUp">No market insight post has been found.</h2> : undefined
                        }
                        {
                            this.state.items?.items?.map(
                                (item, index) => (
                                    <Card hoverable key={index} className="item wowo fadeInUp animated"
                                          style={{animationDelay: `${0.2 * (index + 1)}s`}}
                                          title={null}
                                          bodyStyle={{padding: 0}}
                                    >
                                        <Link to={`/market-insight/${item?.slug}/`}>
                                            <div className={'image'}>
                                                <img src={item?.image} alt={item?.title} />
                                                <div className="hover">
                                                    <span className={'clear-right'}>Read more</span>
                                                </div>
                                            </div>
                                        </Link>
                                        <div className="title">
                                            <Link to={`/market-insight/${item?.slug}/`}>{item?.title}</Link>
                                        </div>
                                    </Card>
                                )
                            )
                        }
                    </div>
                    <div className={'pager'}>
                        <ul>
                            {
                                Utils.numberIterator(1, this.state.items?.total, 1).map(
                                    (item) => (
                                        <li key={item} className={this.state.page === item ? 'active': ''}>
                                            <span onClick={() => this.refreshList(item)}>{item}</span>
                                        </li>
                                    )
                                )
                            }
                        </ul>
                    </div>
                </div>
                <PageFooterComponent />
            </div>
        );
    }
}

export default withRouter(MarketInsightListView);
