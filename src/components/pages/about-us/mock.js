export const AboutUsMockData = {
    blocks: [
        {
            orientation: 'left',
            title: 'About Common Realty',
            image: 'https://api.commonrealty.com.au/media/pages/unknown/WechatIMG236.jpeg',
            imageTitle: 'Connect',
            content: '<p>Common Realty is the one-stop destination that can help at all stages of your property journey.</p> ' +
                '<p>We are the only Sydney based real estate agency that provides complete property services for Asia-Pacific buyers.</p>'
        },
        {
            orientation: 'right',
            title: 'Who we are',
            image: 'https://api.commonrealty.com.au/media/pages/unknown/WechatIMG237.jpeg',
            imageTitle: 'People',
            content: '<p>The team at Common Realty have an enriched cultural background from both Hong Kong and Sydney. Our profound cultural understanding of both cities allows us to build trusted relationships with Hong Kong buyers. As locals in Sydney, we provide personalised, reliable and clear property advice for our clients.</p>'
        },
        {
            orientation: 'left',
            title: 'Results driven',
            image: 'https://api.commonrealty.com.au/media/pages/unknown/WechatIMG238.jpeg',
            imageTitle: 'Case Study',
            content: '<p>Our language proficiency in Cantonese, Chinese and English helps buyers with overcoming the language and cultural barrier that many may face during their property journey.</p>' +
                '<p>Whether you are an investor or looking to buy your new home, with over 100 residential projects on portfolio, we have every confidence in finding the best suited property for you.</p>'
        },
    ]
};
