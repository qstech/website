import {withRouter} from "react-router";
import React from "react";
import PagePaddingComponent from "../../page-padding/page-padding";
import {AboutUsMockData} from "./mock";
import "./about-us.scss";
import PageFooterComponent from "../../page-footer/page-footer";
import {Helmet} from "react-helmet";

const {Component} = require("react");

class AboutUsPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pageData: AboutUsMockData.blocks
        }
    }
    render() {
        return (
            <div className={'about-us-container'}>
                <PagePaddingComponent />
                <Helmet><title>About Us</title></Helmet>
                <div className={'wrapper fixed-width-container'}>
                    <div className={'title'}>
                        <div className={'left wowo animated fadeInUp'}>
                            <h1>About Us</h1>
                        </div>
                    </div>
                    <div className={'list'}>
                        {
                            this.state.pageData?.map(
                                (item, index) => (
                                    <div key={index} className={'item'}>
                                        <div className={`image ${item.orientation === 'left' ? 'img-left' : 'img-right'}`}>
                                            <h2 className={'top wowo animated fadeInUp'}>{item.imageTitle}</h2>
                                            <img className={`wowo animated ${item.orientation === 'left' ? 'fadeInLeft' : 'fadeInRight'}`} src={item.image} alt={item.title} />
                                            <h2 className={'bottom wowo animated fadeInUp'}>{item.imageTitle}</h2>
                                        </div>
                                        <div className={`text wowo animated ${item.orientation !== 'left' ? 'fadeInLeft text-left' : 'fadeInRight text-right'}`}>
                                            <div className={'inner'}>
                                                <div className={'content'}>
                                                    <h3>{item.title}</h3>
                                                    <div className={'body'} dangerouslySetInnerHTML={{__html: item.content}} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            )
                        }
                    </div>
                </div>
                <PageFooterComponent />
            </div>
        );
    }
}

export default withRouter(AboutUsPage);
