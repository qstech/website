import {Component} from "react";
import PagePaddingComponent from "../../page-padding/page-padding";
import {Helmet} from "react-helmet";
import axiosService from "../../../services/axios.service";
import {withRouter} from "react-router";

class TC extends Component {
    constructor(m) {
        super(m);
        this.state = {
            page_info: ''
        }
    }
    componentDidMount() {
        axiosService().post('pages/', {scopes: ['disclaimer']}).then(
            res => {
                console.log(res)
                this.setState({page_info: res.data.data})
            }
        )
    }

    render() {
        return (
            <div className='our-team-page-container'>>
                <PagePaddingComponent />
                <Helmet><title>Terms & Conditions</title></Helmet>
                <div className={'our-team-list-section fixed-width-container wrap'}>
                    <div className="title wowo fadeInUp animated">
                        <div className="left">
                            <h1>Terms & Conditions</h1>
                        </div>
                    </div>
                    <div dangerouslySetInnerHTML={{__html: this.state.page_info.terms_and_conditions?.value}} />
                </div>
            </div>
        )
    }
}

export default withRouter(TC);
