import React, {Component} from "react";
import {Route, Switch, withRouter} from "react-router";
import PageLoadingComponent from "../../page-loading/page-loading";
import PageNotFound from "../../page-error/page-not-found";
import {Helmet} from "react-helmet";
import "./projects.scss";

class ProjectsComponent extends Component {

    render() {
        return (
            <React.Suspense fallback={<PageLoadingComponent />}>
                <Helmet><title>Projects</title></Helmet>
                <Switch>
                    <Route exact path={'/projects/'} component={React.lazy(() => import('./project-list'))} />
                    <Route exact path={'/projects/:slug'} component={React.lazy(() => import('./project-details'))} />
                    <Route render={()=> <PageNotFound />} />
                </Switch>
            </React.Suspense>
        );
    }
}

export default withRouter(ProjectsComponent);
