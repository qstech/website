import React, {Component} from "react";
import "./map-view.scss";
import GoogleMapReact from "google-map-react";
import config from "../../../../common/config";
import {List, Spin, Drawer, Row, Col, Tooltip} from 'antd';
import {Link} from "react-router-dom";
import ProjectSuiteListComponent from "../common/suite-list";
import axiosService from "../../../../services/axios.service";
import PageLoadingComponent from "../../../page-loading/page-loading";
import LoginModal from "../../../modals/login-modal";
import Modals from "../../../../layouts/modals";

export default class ProjectsMapViewComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            drawerVisible: false,
            projectObject: undefined,
        }
        this.mapRef = React.createRef();
    }

    toggleDrawer = (projectSlug = undefined) => {
        if (projectSlug && typeof projectSlug == "string") {
            this.setState({
                ...this.state,
                drawerVisible: true,
            });
            axiosService().get(`projects/${projectSlug}/?map=true`).then(
                res => {
                    if (res.status === 200) {
                        this.setState({
                            ...this.state,
                            projectObject: res.data
                        });
                    }
                }
            );
        } else {
            this.setState({
                ...this.state,
                drawerVisible: false,
                projectObject: undefined
            });
        }
    }

    render() {
        const {projectObject} = this.state;
        return (
            <div className={'projects-map-wrap'}>
                <Drawer
                    visible={this.state.drawerVisible}
                    onClose={this.toggleDrawer}
                    drawStyle={{position: "absolute"}}
                    bodyStyle={{padding: 0}}
                    width={'30vw'}
                    getContainer={() => this.mapRef.current}
                    destroyOnClose={true}
                    placement={'right'}
                >
                    <div className={'project-detail-wrap'}>
                        <Spin spinning={!projectObject}>
                            <Link to={`/projects/${projectObject?.slug}/`}>
                                <div className="image">
                                    <img src={projectObject?.image} alt={projectObject?.name} />
                                    <div className="hover">
                                        <span className={'clear-right'}>View details</span>
                                    </div>
                                </div>
                            </Link>
                            <Row gutter={'64px'}>
                                <Col span={24} className={'content-wrap'}>
                                    <div className={'general-info'}>
                                        <div className={'title'}>
                                            <h2>{projectObject?.name}</h2>
                                        </div>
                                        <div className={'info'}>
                                            <div className={'address'}>
                                                <p><b>{projectObject?.address}</b></p>
                                                <p>{projectObject?.suburb}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={'summary'} dangerouslySetInnerHTML={{__html: projectObject?.highlights}} />
                                    <ProjectSuiteListComponent suiteListObject={projectObject?.suites} hidePrice={projectObject?.hide_price} projectObject={projectObject} />
                                </Col>
                            </Row>
                        </Spin>
                    </div>
                </Drawer>
                <div className={'map-project-list-wrap'}>
                    <h2>Projects in this area</h2>
                    {
                        this.props.mapListLoading ?
                            <PageLoadingComponent /> :
                            <List
                                itemLayout="horizontal"
                                dataSource={this.props.mapListProjects}
                                renderItem={(item) => (
                                    <List.Item>
                                        <div
                                            className={'list-item'}
                                            onClick={
                                                () => this.toggleDrawer(item?.slug)
                                            }
                                        >
                                            <List.Item.Meta
                                                avatar={<img src={item.image} alt={item.name} />}
                                                title={item.name}
                                                description={item.address}
                                            />
                                        </div>
                                    </List.Item>
                                )}
                            />
                    }
                </div>
                <div id={'map-wrap'} ref={this.mapRef}>
                    <GoogleMapReact
                        bootstrapURLKeys={{key: config.googleMapKey}}
                        defaultZoom={11}
                        defaultCenter={{lat: -33.7967241, lng: 151.185157}}
                        options={
                            {
                                styles: config.googleMapStyleContact,
                                draggable: true,
                                mapTypeControl: true
                            }
                        }
                        onChange={this.props.onChange}
                    >
                        {
                            this.props.projects?.map(
                                (item, index) => (
                                    <div key={index} lat={item['coordinates'].lat} lng={item['coordinates'].lng}
                                         style={{height: '35px', width: '25px', position: 'absolute', top: '-35px', left: '-12,5px'}}
                                         onClick={() => item.visible ? this.toggleDrawer(item.slug) : Modals.create(<LoginModal />)}
                                    >
                                        <Tooltip title={item.visible ? item.name : 'Please login to view this project'}>
                                            <svg className={`single-marker ${!item.visible ? 'disabled': ''} ${item.slug === projectObject?.slug ? 'selected': ''}`}
                                                 width="25px" height="35px" viewBox="0 0 25 35" version="1.1">
                                                <title>Shape</title>
                                                <desc>Created with Sketch.</desc>
                                                <defs />
                                                <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                                    <g id="marker-2" fill="#00AE9E" fillRule="nonzero">
                                                        <path xmlns="http://www.w3.org/2000/svg" d="M12.51 35C12.51 35 25 17.7 25 12.02C25 3.65 18.67 0.01 12.5 0C6.33 0.01 0 3.65 0 12.02C0 17.7 12.49 35 12.49 35C12.49 35 12.5 34.98 12.5 34.98C12.5 34.99 12.51 35 12.51 35ZM12.51 7.63C14.9 7.63 16.84 9.38 16.84 11.54C16.84 13.7 14.89 15.45 12.5 15.45C10.11 15.45 8.17 13.7 8.17 11.54C8.17 9.38 10.11 7.63 12.51 7.63Z" id="shape"/>
                                                    </g>
                                                </g>
                                            </svg>
                                        </Tooltip>
                                    </div>
                                )
                            )
                        }
                    </GoogleMapReact>
                </div>
            </div>
        )
    }
}
