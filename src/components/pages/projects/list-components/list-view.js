import React, {Component} from "react";
import "./list-view.scss";
import {Card} from "antd";
import {Link} from "react-router-dom";
import townhouseIcon from '../../../../assets/icons/townhouse.svg';
import apartmentIcon from '../../../../assets/icons/apartment.svg';
import houseIcon from '../../../../assets/icons/house.svg';
import ProjectSuiteListComponent from "../common/suite-list";

export default class ProjectsListViewComponent extends Component {

    propertyTypeSymbol = (type) => {
        switch (type) {
            case 1:
                return (<div><img src={apartmentIcon} alt={''} />Apartments</div>);
            case 2:
                return (<div><img src={townhouseIcon} alt={''} />Townhouses</div>);
            case 3:
                return (<div><img src={houseIcon} alt={''} />Houses</div>);
            case 4:
                return (<div><img src={houseIcon} alt={''} />Duplex</div>);
            default:
                return (<div><img src={houseIcon} alt={''} />Other</div>);
        }
    }

    render() {
        return (
            <div className={'projects-list-wrap'}>
                <div className={'grid inner-wrap'}>
                    {
                        this.props.projects?.length === 0 ?
                            <h2 className="no-project wowo animated fadeInUp">No project found matching search criteria.</h2>
                            :
                            this.props.projects?.map(
                                (item, index) => (
                                    <Card
                                        className={'item wowo animated fadeInUp'}
                                        key={index} title={null}
                                        hoverable
                                        bodyStyle={{padding: 0}}
                                        style={{animationDelay: `${(index + 1) * 0.2}s`}}
                                    >
                                        <Link to={`/projects/${item.slug}/`}>
                                            <div className="image">
                                                <img src={item.image} alt={item.name} />
                                                <div className="hover">
                                                    <span className={'clear-right'}>View details</span>
                                                </div>
                                            </div>
                                            <div className={'content-area'}>
                                                <div className={'general'}>
                                                    <div className={'title'}>
                                                        <h2>{item.name}</h2>
                                                    </div>
                                                    <div className={'info'}>
                                                        <div className={'address'}>
                                                            <span>{item.address}</span>
                                                            <p>{item.suburb}</p>
                                                        </div>
                                                    </div>
                                                    <div className={'bullet-points'} dangerouslySetInnerHTML={{__html: item['highlights']}} />
                                                </div>
                                                <div className={'suites-info'}>
                                                    <ProjectSuiteListComponent suiteListObject={item?.suites} hidePrice={item?.hide_price} projectObject={item} />
                                                    <span className={'update'}>
                                                    <b>Last Update</b> {item.updated ? new Date(item.updated).toLocaleDateString() : null}
                                                </span>
                                                </div>
                                            </div>
                                        </Link>
                                    </Card>
                                )
                            )
                    }
                </div>
            </div>
        );
    }
}
