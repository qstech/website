import React, {Component} from "react";
import searchIcon from "../../../../assets/icons/search.svg";
import {Checkbox, Col, Collapse, Form, Row} from "antd";
import Utils from "../../../../layouts/utils";
import "./filter-bar.scss";

export default class ProjectFilterBarComponent extends Component {
    render() {
        return (
            <Form onValuesChange={this.props.handleFormSubmit} initialValues={this.props.initialValues}>
                <div className={'projects-toolbar'}>
                    <div className={'title'}>
                        <div className={'left'}>
                            <h1>Projects</h1>
                        </div>
                        <div className="right">
                            <div className="switch">
                                <ul>
                                    <li className={this.props.activePanel === 'map' ? 'active' : null}>
                                        <span onClick={() => this.props.handleChangePanel('map')}>
                                            <span style={{marginRight: '20px'}}>Map</span>
                                            <svg x="0px" y="0px" style={{width: '20px', height: '27px'}}>
                                                <circle className="st0" cx="10.1" cy="7.9" r="2.2" />
                                                <path className="st0" d="M17.5,7.9c0,3.2-4.4,9.9-6.4,12.9c-0.5,0.7-1.5,0.7-2,0c-2-2.9-6.4-9.7-6.4-12.9c0-4.1,3.3-7.4,7.4-7.4
                                                    S17.5,3.8,17.5,7.9z" />
                                                <path className="st0" d="M6.5,17.3l-0.4,0.1l-1.2,0.4c-0.2,0.1-0.4,0.2-0.5,0.5l-3.8,6.9c-0.1,0.2,0.1,0.5,0.4,0.4l4.4-2.3l4.2,1.9
                                                    c0.3,0.1,0.6,0.1,0.8,0l4.2-1.9l4.4,2.3c0.3,0.1,0.5-0.2,0.4-0.4l-3.8-6.9c-0.1-0.2-0.3-0.4-0.5-0.5l-1.7-0.5" />
                                            </svg>
                                        </span>
                                    </li>
                                    <li className={this.props.activePanel === 'list' ? 'active' : null}>
                                        <span onClick={() => this.props.handleChangePanel('list')}>
                                            <svg x="0px" y="0px" viewBox="0 0 100 100" style={{width: '22px', height: '22px'}}>
                                                <g>
                                                    <path className="st2" d="M37.8,49H11C4.9,49-0.1,44-0.1,37.8V11.1C-0.1,5,4.9,0,11,0h26.7c6.1,0,11.1,5,11.1,11.1v26.7
                                                        C48.9,44,43.9,49,37.8,49z M11,4.5c-3.7,0-6.7,3-6.7,6.7v26.7c0,3.7,3,6.7,6.7,6.7h26.7c3.7,0,6.7-3,6.7-6.7V11.1
                                                        c0-3.7-3-6.7-6.7-6.7H11z"/>
                                                    <path className="st2" d="M89,49H62.2c-6.1,0-11.1-5-11.1-11.1V11.1C51.1,5,56.1,0,62.2,0H89c6.1,0,11.1,5,11.1,11.1v26.7
                                                        C100.1,44,95.1,49,89,49z M62.2,4.5c-3.7,0-6.7,3-6.7,6.7v26.7c0,3.7,3,6.7,6.7,6.7H89c3.7,0,6.7-3,6.7-6.7V11.1
                                                        c0-3.7-3-6.7-6.7-6.7H62.2z"/>
                                                    <path className="st2" d="M37.8,100H11c-6.1,0-11.1-5-11.1-11.1V62.2C-0.1,56,4.9,51,11,51h26.7c6.1,0,11.1,5,11.1,11.1v26.7
                                                        C48.9,95,43.9,100,37.8,100z M11,55.5c-3.7,0-6.7,3-6.7,6.7v26.7c0,3.7,3,6.7,6.7,6.7h26.7c3.7,0,6.7-3,6.7-6.7V62.2
                                                        c0-3.7-3-6.7-6.7-6.7H11z"/>
                                                    <path className="st2" d="M89,100H62.2c-6.1,0-11.1-5-11.1-11.1V62.2c0-6.1,5-11.1,11.1-11.1H89c6.1,0,11.1,5,11.1,11.1v26.7
                                                        C100.1,95,95.1,100,89,100z M62.2,55.5c-3.7,0-6.7,3-6.7,6.7v26.7c0,3.7,3,6.7,6.7,6.7H89c3.7,0,6.7-3,6.7-6.7V62.2
                                                        c0-3.7-3-6.7-6.7-6.7H62.2z"/>
                                                </g>
                                            </svg>
                                            <span style={{marginLeft: '20px'}}>List</span>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                            <div className="search">
                                <Form.Item name={'search'}>
                                    <input type="text" placeholder="Search by state, suburb or postcode" />
                                </Form.Item>
                                <button>
                                    <img src={searchIcon} alt={''} />
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className={'filters'}>
                        <div className={'wrap'}>
                            <div className={'filter-form'}>
                                <div className={'items'}>
                                    <div className={'left-section selectors'}>
                                        <h4>Search</h4>
                                        <div>
                                            <label>
                                                <p>Bedroom</p>
                                                <Form.Item name={'bedroom'}>
                                                    <select>
                                                        {
                                                            this.props.filters?.bedroom?.map(
                                                                (item, index) => (
                                                                    <option key={index} value={item.value}>{item.text}</option>
                                                                )
                                                            )
                                                        }
                                                    </select>
                                                </Form.Item>
                                            </label>
                                            <label>
                                                <p>Property Type</p>
                                                <Form.Item name={'property_type'}>
                                                    <select>
                                                        {
                                                            this.props.filters?.property_type?.map(
                                                                (item, index) => (
                                                                    <option key={index} value={item.value}>{item.text}</option>
                                                                )
                                                            )
                                                        }
                                                    </select>
                                                </Form.Item>
                                            </label>
                                            <div className={'price-range'}>
                                                <label>
                                                    <p>Min Price</p>
                                                    <Form.Item name={'min_price'}>
                                                        <select name={'min_price'}>
                                                            <option key={0} value={0}>Any</option>
                                                            {
                                                                Utils.numberIterator(500000, this.props.filters.min_price, 50000).map(
                                                                    (item, index) => {
                                                                        return (
                                                                            <option key={index} value={item}>
                                                                                {
                                                                                    Utils.currencyTransformer(item)
                                                                                }
                                                                            </option>
                                                                        )}
                                                                )
                                                            }
                                                        </select>
                                                    </Form.Item>
                                                </label>
                                                <label>
                                                    <p>Max Price</p>
                                                    <Form.Item name={'max_price'}>
                                                        <select name={'max_price'}>
                                                            <option key={0} value={0}>Any</option>
                                                            {
                                                                Utils.numberIterator(500000, this.props.filters.max_price, 50000).map(
                                                                    (item, index) => {
                                                                        return (
                                                                            <option key={index} value={item}>
                                                                                {
                                                                                    Utils.currencyTransformer(item)
                                                                                }
                                                                            </option>
                                                                        )}
                                                                )
                                                            }
                                                        </select>
                                                    </Form.Item>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={'right-section selectors'}>
                                        <h4>Advanced Filters</h4>
                                        <div>
                                            <label>
                                                <p>Bathroom</p>
                                                <Form.Item name={'bathroom'}>
                                                    <select name={'bathroom'}>
                                                        {
                                                            this.props.filters?.bathroom?.map(
                                                                (item, index) => (
                                                                    <option key={index} value={item.value}>{item.text}</option>
                                                                )
                                                            )
                                                        }
                                                    </select>
                                                </Form.Item>
                                            </label>
                                            <label>
                                                <p>Car Space</p>
                                                <Form.Item name={'car_space'}>
                                                    <select name={'car_space'}>
                                                        {
                                                            this.props.filters?.car_space?.map(
                                                                (item, index) => (
                                                                    <option key={index} value={item.value}>{item.text}</option>
                                                                )
                                                            )
                                                        }
                                                    </select>
                                                </Form.Item>

                                            </label>
                                            <label>
                                                <p>Special Promotion</p>
                                                <Form.Item name={'promotion'}>
                                                    <select name={'promotion'}>
                                                        {
                                                            this.props.filters?.promotion?.map(
                                                                (item, index) => (
                                                                    <option key={index} value={item.value}>{item.text}</option>
                                                                )
                                                            )
                                                        }
                                                    </select>
                                                </Form.Item>
                                            </label>
                                        </div>
                                    </div>
                                    <Collapse
                                        className={'more-btn'}
                                        ghost
                                        accordion={true}
                                        expandIcon={() => <span className={'btn'}>More Refinements</span>}
                                        expandIconPosition={'right'}
                                    >
                                        <Collapse.Panel header={null} forceRender={true}>
                                            <div className={'container'}>
                                                <div className="select-wrap">
                                                    <h4>Investment</h4>
                                                    <Form.Item name={'investment_options'}>
                                                        <Checkbox.Group className="options">
                                                            <Row>
                                                                {
                                                                    this.props.filters?.investment?.map(
                                                                        (item, index) => (
                                                                            <Col key={index} span={6} lg={6} xs={24} sm={24} md={24}>
                                                                                <Checkbox value={item.value}>{item.text}</Checkbox>
                                                                            </Col>
                                                                        )
                                                                    )
                                                                }
                                                            </Row>
                                                        </Checkbox.Group>
                                                    </Form.Item>
                                                </div>
                                                <div className="select-wrap">
                                                    <h4>Completion date</h4>
                                                    <Form.Item name={'completion_options'}>
                                                        <Checkbox.Group className="options">
                                                            <Row>
                                                                {
                                                                    this.props.filters?.completion?.map(
                                                                        (item, index) => (
                                                                            <Col key={index} span={6} lg={6} xs={24} sm={24} md={24}>
                                                                                <Checkbox value={item.value}>{item.text}</Checkbox>
                                                                            </Col>
                                                                        )
                                                                    )
                                                                }
                                                            </Row>
                                                        </Checkbox.Group>
                                                    </Form.Item>
                                                </div>
                                            </div>
                                        </Collapse.Panel>
                                    </Collapse>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Form>
        );
    }
}
