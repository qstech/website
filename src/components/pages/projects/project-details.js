import {withRouter} from "react-router";
import React, {Component} from "react";
import "./project-details.scss";
import ProjectDetailSliderComponent from "./detail-components/slider";
import ProjectDetailsContentComponent from "./detail-components/detail-content";
import {Helmet} from "react-helmet";
import PageFooterComponent from "../../page-footer/page-footer";
import footerBitmap from "../../../assets/footer-bitmap.png";
import axiosService from "../../../services/axios.service";
import PageNotFound from "../../page-error/page-not-found";
import NewsSectionComponent from "../common/new-section/news-section";

class ProjectDetailsComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            projectObject: undefined,
            pageNotFound: false
        }
    }

    componentDidMount() {
        const slug = this.props.match.params.slug;
        axiosService().get(`projects/${slug}/`).then(
            res => {
                if (res.status === 200) {
                    this.setState({
                        ...this.state,
                        projectObject: res.data,
                        pageNotFound: false
                    })
                } else {
                    this.setState({
                        projectObject: undefined,
                        pageNotFound: true
                    })
                }
            }
        )
    }

    render() {
        const {projectObject} = this.state;
        return this.state.pageNotFound ? <PageNotFound /> : (
            <div className={'project-details'}>
                <Helmet><title>{projectObject?.name || 'loading'} | Projects</title></Helmet>
                <ProjectDetailSliderComponent slides={projectObject?.images} />
                <ProjectDetailsContentComponent projectObject={projectObject} />
                <NewsSectionComponent />
                <PageFooterComponent styles={{background: `rgba(232, 232, 232, 0.4) url(${footerBitmap}) no-repeat`}} />
            </div>
        )
    }
}

export default withRouter(ProjectDetailsComponent);
