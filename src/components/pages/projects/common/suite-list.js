import React, {Component} from "react";
import bedroom from "../../../../assets/icons/bedroom.svg";
import bathroom from "../../../../assets/icons/bathroom.svg";
import carSpace from "../../../../assets/icons/carport.svg";
import Utils from "../../../../layouts/utils";
import apartmentIcon from "../../../../assets/icons/apartment.svg";
import townhouseIcon from "../../../../assets/icons/townhouse.svg";
import houseIcon from "../../../../assets/icons/house.svg";
import "./suite-list.scss";
import Modals from "../../../../layouts/modals";
import ContactAgentModal from "../../../modals/contact-agent-modal";

export default class ProjectSuiteListComponent extends Component {
    propertyTypeSymbol = (type) => {
        switch (type) {
            case 1:
                return (<div><img src={apartmentIcon} alt={''} />Apartments</div>);
            case 2:
                return (<div><img src={townhouseIcon} alt={''} />Townhouses</div>);
            case 3:
                return (<div><img src={houseIcon} alt={''} />Houses</div>);
            case 4:
                return (<div><img src={houseIcon} alt={''} />Duplex</div>);
            default:
                return (<div><img src={houseIcon} alt={''} />Other</div>);
        }
    }

    handleContactAgent = () => {
        Modals.create(<ContactAgentModal project={this.props.projectObject?.slug} />);
    }

    render() {
        const {suiteListObject, hidePrice} = this.props;
        return (
            <div className={'suites-list'}>
                {
                    suiteListObject?.map(
                        (typeCollection, typeIndex) => (
                            <div className={'suites'} key={typeIndex}>
                                <div className={'type'}>{this.propertyTypeSymbol(typeCollection.type)}</div>
                                {
                                    typeCollection.items.map(
                                        (suite, suiteIndex) => (
                                            <div className={'entry'} key={suiteIndex}>
                                                <div className={'listing'}>
                                                    <div><img src={bedroom} alt={''}/>{suite.bedroom === 0.5 ? 'Studio' : suite.bedroom}</div>
                                                    <div><img src={bathroom} alt={''}/>{suite.bathroom}</div>
                                                    <div><img src={carSpace} alt={''}/>{suite.car_space}</div>
                                                </div>
                                                <div className={'price'}>
                                                    {hidePrice ? <a className='contact-agent-btn' onClick={this.handleContactAgent}>Request Price</a> : Utils.currencyTransformer(suite.min) + ' - ' + Utils.currencyTransformer(suite.max)}
                                                </div>
                                            </div>
                                        )
                                    )
                                }
                            </div>
                        )
                    )
                }
            </div>
        );
    }
}
