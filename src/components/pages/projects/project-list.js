import React, {Component} from "react";
import "./project-list.scss";
import {withRouter} from "react-router";
import ProjectsMock from "./mock";
import ProjectFilterBarComponent from "./list-components/filter-bar";
import ProjectsListViewComponent from "./list-components/list-view";
import PageFooterComponent from "../../page-footer/page-footer";
import footerBitmap from "../../../assets/footer-bitmap.png";
import {Tabs} from "antd";
import ProjectsMapViewComponent from "./list-components/map-view";
import PagePaddingComponent from "../../page-padding/page-padding";
import axiosService from "../../../services/axios.service";
import PageLoadingComponent from "../../page-loading/page-loading";
import AuthService from "../../../services/auth.service";
import Modals from "../../../layouts/modals";
import LoginModal from "../../modals/login-modal";
import { parse } from 'qs';
import projects from "./projects";

class ProjectListComponent extends Component {
    constructor(props) {
        super(props);
        const presets = parse(this.props.location.search.substring(1));
        this.state = {
            activePanel: 'list',
            filters: ProjectsMock.filters,
            initialValues: presets.length > 0 ? presets : {
                'bedroom': 'all',
                'property_type': 'all',
                'min_price': 0,
                'max_price': 0,
                'bathroom': 'all',
                'car_space': 'all',
                'promotion': 'all',
                'completion_options': [],
                'investment_options': [],
                'search': ''
            },
            projects: undefined,
            mapProjects: undefined,
            loading: true,
            filterValues: undefined,
            mapListProjects: undefined,
            mapListLoading: true,
            mapBounds: undefined,
            showLogin: false
        }
        if (presets.length > 0) {
            this.setState({
                ...this.state,
                initialValues: presets,
                filterValue: presets
            })
        }
        this.getData(presets || {});
    }

    getData = (dataObject) => {
        this.setState({
            ...this.state,
            loading: true,
            mapListLoading: true,
        });

        axiosService().post('projects/', dataObject).then(
            res => {
                if (res.status === 200) {
                    this.setState({
                        ...this.state,
                        projects: res.data,
                        loading: false
                    });
                }
            }
        );
        axiosService().post('projects/', {map: true, ...dataObject}).then(
            res => {
                if (res.status === 200) {
                    this.setState({
                        ...this.state,
                        mapProjects: res.data,
                    });
                }
            }
        );
        axiosService().post('projects/', {map: false, map_boundary: this.state.mapBounds, ...dataObject}).then(
            res => {
                if (res.status === 200) {
                    this.setState({
                        ...this.state,
                        mapListProjects: res.data,
                        mapListLoading: false
                    });
                }
            }
        );
    }

    handleFormSubmit = (_, values) => {
        this.setState({
            ...this.state,
            filterValues: values
        })
        this.getData(values);
    }

    handleMapBoundaryChange = (boundaries) => {
        const bounds = {
            lat: {
                min: boundaries.bounds.sw.lat,
                max: boundaries.bounds.ne.lat
            },
            lng: {
                min: boundaries.bounds.sw.lng,
                max: boundaries.bounds.ne.lng
            }
        }
        this.setState({
            ...this.state,
            mapBounds: bounds,
            mapListProjects: undefined,
            mapListLoading: true
        });
        axiosService().post('projects/', {map: false, map_boundary: bounds, ...this.state.filterValues}).then(
            res => {
                if (res.status === 200) {
                    this.setState({
                        ...this.state,
                        mapListProjects: res.data,
                        mapListLoading: false
                    });
                }
            }
        );
    }

    componentDidMount() {
        AuthService.getCurrentUser().then( res => {
            if (res.status !== 200) {
                this.setState({
                    showLogin: true
                })
            }
        });
    }

    render() {
        return (
            <div className={'project-list'}>
                <PagePaddingComponent />
                <div className={`notification-bar ${this.state.showLogin ? 'visible': ''}`}>
                    <h2>You are currently viewing <b>{this.state.projects?.length}</b> projects, please log in to explore <b>all</b> of our featured projects.</h2>
                    <div>
                        <div className={'login'} onClick={() => Modals.create(<LoginModal />)}>Log in</div>
                        <div className={'close-btn'} onClick={() => this.setState({showLogin: false})}>Close</div>
                    </div>
                </div>
                <ProjectFilterBarComponent
                    filters={this.state.filters}
                    initialValues={this.state.initialValues}
                    handleFormSubmit={this.handleFormSubmit}
                    activePanel={this.state.activePanel}
                    handleChangePanel={(panelKey) => this.setState({...this.state, activePanel: panelKey})}
                />
                <Tabs
                    animated={{ inkBar: true, tabPane: true }}
                    activeKey={this.state.activePanel || 'list'}
                >
                    <Tabs.TabPane key={'list'}>
                        {
                            this.state.loading ?
                                <PageLoadingComponent /> :
                                <ProjectsListViewComponent projects={this.state.projects} />
                        }
                        <PageFooterComponent styles={{background: `rgba(232, 232, 232, 0.4) url(${footerBitmap}) no-repeat`}} />
                    </Tabs.TabPane>
                    <Tabs.TabPane key={'map'}>
                        <ProjectsMapViewComponent
                            projects={this.state.mapProjects}
                            filterValues={this.state.filterValues}
                            onChange={this.handleMapBoundaryChange}
                            mapListProjects={this.state.mapListProjects}
                            mapListLoading={this.state.mapListLoading}
                        />
                    </Tabs.TabPane>
                </Tabs>
            </div>
        )
    }
}

export default withRouter(ProjectListComponent);
