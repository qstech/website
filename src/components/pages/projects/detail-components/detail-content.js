import React, {Component} from "react";
import "./detail-content.scss";
import {Badge} from "antd";
import ProjectSuiteListComponent from "../common/suite-list";
import GoogleMapReact from "google-map-react";
import config from "../../../../common/config";
import googleMapMarker from "../../../../assets/icons/google-map-marker.svg";
import Modals from "../../../../layouts/modals";
import ContactAgentModal from "../../../modals/contact-agent-modal";
import PageLoadingComponent from "../../../page-loading/page-loading";
import {GoogleMap, LoadScript, Marker} from "@react-google-maps/api";

export default class ProjectDetailsContentComponent extends Component {
    handleContactAgent = () => {
        Modals.create(<ContactAgentModal project={this.props.projectObject?.slug} />);
    }

    render() {
        const {projectObject} = this.props;
        return !projectObject ? <PageLoadingComponent /> : (
            <div className={'project-details-content-wrap wowo animated fadeInUp delay-2'}>
                <div className={'inner-wrap'}>
                    <div className="title">
                        <div className="left">
                            <h1>{this.props.projectObject?.name}</h1>
                        </div>
                    </div>
                    <div className={'description'} dangerouslySetInnerHTML={{__html: projectObject?.description}} />
                    <div className={'attributes'}>
                        {
                            projectObject?.attributesList?.map(
                                (item, index) => (
                                    <div key={index} className={'attribute-item'}>
                                        <img src={item.icon} alt={item.name} />
                                        {item.name}
                                    </div>
                                )
                            )
                        }
                    </div>
                    <div className={'btn-container'}>
                        {
                            projectObject?.promotion === 1 ?
                            <p><Badge status={'success'} text={'On Promotion'} /></p> :
                            null
                        }
                        <span className={'contact-agent-btn clear-right'} onClick={this.handleContactAgent}>Contact Agent</span>
                    </div>
                    <div className={'left-info wowo animated fadeInUp delay-4'}>
                        <h3>Highlights</h3>
                        <div className={'summary'} dangerouslySetInnerHTML={{__html: projectObject?.highlights}} />
                        <h3>Price detail</h3>
                        <ProjectSuiteListComponent suiteListObject={projectObject?.suites} hidePrice={projectObject?.hide_price} projectObject={projectObject} />
                        <h3>Downloadable content</h3>
                        <div className={'download'}>
                            <ul>
                                {
                                    projectObject?.files?.map(
                                        (item, index) => (
                                            <li key={index}>
                                                <a href={item.link} download>{item.name}</a>
                                            </li>
                                        )
                                    )
                                }
                            </ul>
                        </div>
                    </div>
                    <div className={'right-info wowo animated fadeInUp delay-6'}>
                        <h3>Property Info</h3>
                        <div className={'property-info'}>
                            <div className={'location'}>
                                <div className={'address'}><b>{projectObject?.address}</b></div>
                                <div className={'suburb'}>{projectObject?.suburb}</div>
                            </div>
                            <div className={'property-details'}>
                                <div>
                                    <p><strong>Developer</strong></p>
                                    <p>{projectObject?.developer}</p>
                                </div>
                                <div>
                                    <p><strong>Architect</strong></p>
                                    <p>{projectObject?.architect || '--'}</p>
                                </div>
                                <div>
                                    <p><strong>Builder</strong></p>
                                    <p>{projectObject?.builder || '--'}</p>
                                </div>
                                <div>
                                    <p><strong>Product Mix</strong></p>
                                    <p>{projectObject?.product_mix}</p>
                                </div>
                                <div>
                                    <p><strong>Estimated Completion</strong></p>
                                    <p>
                                        {projectObject?.estimated_completion ?
                                            new Date(projectObject?.estimated_completion).toLocaleDateString()
                                            :
                                            'Not Specified'
                                        }
                                    </p>
                                </div>
                                <div>
                                    <p><strong>Last Update</strong></p>
                                    <p>
                                        {projectObject?.updated ?
                                            new Date(projectObject?.updated).toLocaleDateString()
                                            :
                                            'Not Specified'
                                        }
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={'map-wrap wowo animated fadeInUp delay-6'}>
                        <h3>Location</h3>
                        <div className={'map-container'}>
                            <LoadScript googleMapsApiKey={config.googleMapKey}>
                                <GoogleMap
                                    mapContainerStyle={{width: '100%', height: '100%'}}
                                    zoom={16}
                                    center={{lat: projectObject?.location?.lat, lng: projectObject?.location?.lng}}
                                    options={{styles: config.googleMapStyleContact, draggable: false}}
                                >
                                    <Marker
                                        position={projectObject?.location}
                                        onClick={() => window.open(
                                            `https://www.google.com/maps/place/${projectObject?.location?.lat},${projectObject?.location?.lng}`,
                                            '_blank'
                                        )}
                                        icon={googleMapMarker}
                                    />
                                </GoogleMap>
                            </LoadScript>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
