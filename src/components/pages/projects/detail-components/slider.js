import React, {Component} from "react";
import Slider from "react-slick";
import "./slider.scss";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import {Image} from "antd";

export default class ProjectDetailSliderComponent extends Component {
    render() {
        return (
            <div className={'project-details-slider-wrap wowo animated fadeInUp'}>
                <Slider
                    settings={{
                        dots: false,
                        infinite: true,
                        speed: 300,
                        slidesToShow: 3,
                        lazyLoad: true,
                    }}
                >
                    {
                        this.props.slides?.items?.map(
                            (item, index) => (
                                <div key={index}>
                                    <Image src={item} alt={this.props.slides.name} />
                                </div>
                            )
                        )
                    }
                </Slider>
            </div>
        );
    }
}
