import PageLoadingComponent from "../../page-loading/page-loading";
import {Helmet} from "react-helmet";
import {Route, Switch, withRouter} from "react-router";
import PageNotFound from "../../page-error/page-not-found";
import React, {Component} from "react";

class MediaComponent extends Component {
    render() {
        return (
            <React.Suspense fallback={<PageLoadingComponent />}>
                <Helmet><title>Media</title></Helmet>
                <Switch>
                    <Route exact path={'/media/'} component={React.lazy(() => import('./media-list'))} />
                    <Route exact path={'/media/:slug'} component={React.lazy(() => import('./media-details'))} />
                    <Route render={()=> <PageNotFound />} />
                </Switch>
            </React.Suspense>
        );
    }
}

export default withRouter(MediaComponent);
