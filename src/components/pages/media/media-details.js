import {withRouter} from "react-router";
import React, {Component} from "react";
import ArticleDetailComponent from '../common/article-details/article-details'
import axiosService from "../../../services/axios.service";
import PageNotFound from "../../page-error/page-not-found";
import './media-details.scss';

class MediaDetailPageComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mediaObject: undefined,
            pageNotFound: false
        }
    }

    componentDidMount() {
        const slug = this.props.match.params.slug;
        axiosService().get(
            `articles/media/${slug}`
        ).then(
            res => {
                if (res.status === 200) {
                    this.setState({
                        ...this.state,
                        mediaObject: res.data,
                        pageNotFound: false
                    })
                } else {
                    this.setState({
                        mediaObject: undefined,
                        pageNotFound: true
                    })
                }
            }
        );
    }

    render() {
        return this.state.pageNotFound ? <PageNotFound /> : <ArticleDetailComponent articleObject={this.state.mediaObject} objectType={'media'} />;
    }
}

export default withRouter(MediaDetailPageComponent);
