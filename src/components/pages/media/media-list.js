import {withRouter} from "react-router";
import React, {Component} from "react";
import "./media-list.scss";
import PagePaddingComponent from "../../page-padding/page-padding";
import axiosService from "../../../services/axios.service";
import PageLoadingComponent from "../../page-loading/page-loading";
import {Link} from "react-router-dom";
import Utils from "../../../layouts/utils";
import PageFooterComponent from "../../page-footer/page-footer";
import footerBitmap from "../../../assets/footer-bitmap.png";
import {Card} from "antd";

class MediaListPageComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: undefined,
            notFound: false,
            page: 1
        }
    }

    componentDidMount() {
        axiosService().post(`articles/`, {
            page: this.state.page,
            type: 'media',
            items_per_page: 4
        }).then(
            res => {
                if (res.status === 200) {
                    this.setState({
                        items: res.data
                    });
                } else {
                    this.setState({
                        notFound: true
                    });
                }
            }
        );
    }

    refreshList = (pageNumber) => {
        axiosService().post(`articles/`, {
            page: pageNumber,
            type: 'media',
            items_per_page: 4
        }).then(
            res => {
                if (res.status === 200) {
                    this.setState({
                        items: res.data,
                        page: pageNumber
                    });
                } else {
                    this.setState({
                        notFound: true,
                        page: 1
                    });
                }
            }
        );
    }

    scrollToList = () => {
        const listEl = document.getElementById('media-list');
        listEl.scrollIntoView({behavior: "smooth"});
    }

    render() {
        return (
            <div className={'media-list-page'}>
                <PagePaddingComponent />
                <div className={'media-wrap fixed-width-container'}>
                    <div className="title wowo fadeInUp animated">
                        <div className="left">
                            <h1>Media</h1>
                        </div>
                        <div className="right">
                        </div>
                    </div>
                    <div className="feature">
                        <div className={'cover'}>
                            <img src={'https://api.commonrealty.com.au/media/pages/unknown/shutterstock_1025960785-2.jpg'} alt={'Property Media'} />
                        </div>
                        <div className="left wowo fadeInUp animated" style={{animationDelay: '0.2s'}}>
                            <h2 className="top">Market Update</h2>
                            <h2 className="bottom">Market Update</h2>
                        </div>
                        <div className="right wowo fadeInUp animated" style={{animationDelay: '0.4s'}}>
                            <div className="text-block">
                                <h3>Media</h3>
                                <span className={'clear-right'} onClick={this.scrollToList}>Read More</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={'media-list'} id={'media-list'}>
                    <div className={'list-wrap fixed-width-container'}>
                        <div className="title wowo fadeInUp animated">
                            <div className="left">
                                <h2>More news</h2>
                            </div>
                        </div>
                        {
                            !this.state.items?.items ? <PageLoadingComponent /> :
                            <div className={'list wowo animated fadeInUp'}>
                                {
                                    this.state.notFound ? <h2 className="no-data wowo animated fadeInUp">No media post has been found.</h2> : undefined
                                }
                                {
                                    this.state.items?.items?.map(
                                        (item, index) => (
                                            <Card hoverable key={index} className="item wowo fadeInUp animated"
                                                  style={{animationDelay: `${0.2 * (index + 1)}s`}}
                                                  title={null}
                                                  bodyStyle={{padding: 0}}
                                            >
                                                <Link to={`/media/${item?.slug}/`}>
                                                    <div className={'image'}>
                                                        <img src={item?.image} alt={item?.title} />
                                                        <div className="hover">
											                <span className={'clear-right'}>Read more</span>
                                                        </div>
                                                    </div>
                                                </Link>
                                                <div className="title">
                                                    <Link to={`/media/${item?.slug}/`}>{item?.title}</Link>
                                                </div>
                                            </Card>
                                        )
                                    )
                                }
                            </div>
                        }
                        <div className={'pager'}>
                            <ul>
                                {
                                    Utils.numberIterator(1, this.state.items?.total, 1).map(
                                        (item) => (
                                            <li key={item} className={this.state.page === item ? 'active': ''}>
                                                <span onClick={() => this.refreshList(item)}>{item}</span>
                                            </li>
                                        )
                                    )
                                }
                            </ul>
                        </div>
                    </div>
                </div>
                <PageFooterComponent styles={{background: `rgba(232, 232, 232, 0.4) url(${footerBitmap}) no-repeat`}} />
            </div>
        );
    }
}

export default withRouter(MediaListPageComponent);
