import {withRouter} from "react-router";
import React from "react";
import "./services.scss";
import PagePaddingComponent from "../../page-padding/page-padding";
import {ServicesMock} from "./mocks";
import {Helmet} from "react-helmet";
import Utils from "../../../layouts/utils";

const {Component} = require("react");

class ServicesPageComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            serviceData: ServicesMock
        }
    }

    componentDidMount() {
        Utils.scrollToElementID(this.props.location.hash.substring(1));
    }

    componentDidUpdate() {
        Utils.scrollToElementID(this.props.location.hash.substring(1));
    }

    render() {
        const {serviceData} = this.state;
        return (
            <div className={'service-page-container'}>
                <PagePaddingComponent />
                <Helmet><title>Services</title></Helmet>
                <div className={'service-list-section fixed-width-container'}>
                    <div className="title wowo fadeInUp animated">
                        <div className="left">
                            <h1>Services</h1>
                        </div>
                    </div>
                    <div className={'service-block'}>
                        <div className={'left-section wowo animated fadeInLeft'}>
                            <div className={'grid'}>
                                {
                                    serviceData?.serviceList?.list?.map(
                                        (item, index) => (
                                            <div key={index} className={'item'}>
                                                {item.label}
                                            </div>
                                        )
                                    )
                                }
                            </div>
                        </div>
                        <div className={'right-section wowo animated fadeInRight'}>
                            <img src={serviceData?.serviceList?.image} alt={'Property Management Services'} />
                        </div>
                    </div>
                </div>
                <div className={'info-section-top wowo animated fadeInUp'}>
                    <div className={'fixed-width-container'}>
                        <div id={'lease-management'}>
                            <h3>Lease & Management</h3>
                            <div dangerouslySetInnerHTML={{__html: serviceData?.leaseManagement.description}} />
                        </div>
                        <div className={'icon-section'}>
                            {serviceData?.iconSection?.map(
                                (item, index) => (
                                    <div key={index} className={'item'}>
                                        <h4><img src={item.icon} alt={''} />{item.title}</h4>
                                        <div dangerouslySetInnerHTML={{__html: item.body}} />
                                    </div>
                                )
                            )}
                        </div>
                        <div id={'real-estate-advisory'}>
                            <h3>Real estate advisory</h3>
                            <div dangerouslySetInnerHTML={{__html: serviceData?.realEstateAdvisory.description}} />
                            <div id={'project-marketing'}>
                                {
                                    serviceData?.realEstateAdvisory?.projectMarketingList?.map(
                                        (item, index) => (
                                            <div key={index} className={'item'}>
                                                <div className={'left'} />
                                                <div className={'right'}>{item}</div>
                                            </div>
                                        )
                                    )
                                }
                            </div>
                        </div>
                    </div>
                </div>
                <div className={'info-section-bottom fixed-width-container wowo animated fadeInUp'}>
                    {
                        serviceData?.infoSectionList?.map(
                            (item, index) => (
                                <div key={index} id={Utils.convertTextToSlugFormat(item.title)} className={'item'}>
                                    <div className={'text'}>
                                        <h3>{item.title}</h3>
                                        <div dangerouslySetInnerHTML={{__html: item.body}} />
                                    </div>
                                    <div className={'image'}>
                                        <img src={item.image} alt={item.title} />
                                    </div>
                                </div>
                            )
                        )
                    }
                </div>
            </div>
        );
    }
}

export default withRouter(ServicesPageComponent);
