import React, {Component} from "react";
import "./news-section.scss";
import {Link} from "react-router-dom";
import {Card} from "antd";
import axiosService from "../../../../services/axios.service";

export default class NewsSectionComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            news: undefined
        }
    }
    componentDidMount() {
        axiosService().post(`articles/`, this.props.type === 'market-insight' ? {type:'market-insight'} : {}).then(
            res => {
                if (res.status === 200) {
                    this.setState({
                        ...this.state,
                        news: res.data
                    });
                }
            }
        )
    }

    render() {
        const {news} = this.state;
        return (
            <section className={'news-section wowo fadeInUp animated'}>
                <div className={'media fixed-width-container'}>
                    <div className="title has-border">
                        <div className="left">
                            <h2>{ this.props.type === 'market-insight' ? 'Related Market Insights' : 'Related news'}</h2>
                        </div>
                    </div>
                    <div className={'grid'}>
                        {
                            news?.items?.map(
                                (item, index) => (
                                    <Card hoverable key={index} className="item wowo fadeInUp animated"
                                          style={{animationDelay: `${0.2 * (index + 1)}s`}}
                                          title={null}
                                          bodyStyle={{padding: 0}}
                                    >
                                        <Link to={`/${this.props.type || 'media'}/${item?.slug}/`}>
                                            <div className="image">
                                                <img src={item?.image} alt={item?.title} />
                                                <div className="hover">
                                                    <span className={'clear-right'}>Read more</span>
                                                </div>
                                            </div>
                                        </Link>
                                        <div className="title">
                                            <Link to={`/${this.props.type || 'media'}/${item?.slug}/`}>{item?.title}</Link>
                                        </div>
                                    </Card>
                                )
                            )
                        }
                    </div>
                </div>
            </section>
        );
    }
}
