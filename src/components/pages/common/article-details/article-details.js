import PagePaddingComponent from "../../../page-padding/page-padding";
import React, {Component} from "react";
import "./article-details.scss";
import {Image} from "antd";
import {Link} from "react-router-dom";
import axiosService from "../../../../services/axios.service";
import NewsSectionComponent from "../new-section/news-section";
import PageFooterComponent from "../../../page-footer/page-footer";
import footerBitmap from "../../../../assets/footer-bitmap.png";
import Modals from "../../../../layouts/modals";
import LoginModal from "../../../modals/login-modal";
import {Helmet} from "react-helmet";


export default class ArticleDetailComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            featuredProjects: undefined
        }
    }

    componentDidMount() {
        axiosService().post('projects/', {page: 1, items_per_page: 4}).then(
            res => {
                if (res.status === 200) {
                    this.setState({
                        ...this.state,
                        featuredProjects: res.data
                    });
                }
            }
        );
    }

    handleLoginClick = () => {
        if (!this.props.user) {
            Modals.create(<LoginModal />);
        }
    }

    render() {
        const {articleObject} = this.props;
        return (
            <div className={'article-detail-page-container'}>
                <PagePaddingComponent />
                <Helmet><title>{articleObject?.title || 'loading'}</title></Helmet>
                <div className={'inner-wrap fixed-width-container'}>
                    <div className="title wowo fadeInUp animated">
                        <div className="left">
                            <h1>{articleObject?.title}</h1>
                        </div>
                    </div>
                    <div className={'left-section wowo animated fadeInUp'} style={{animationDelay: '0.2s'}}>
                        <div className="cover-img">
                            <Image src={articleObject?.image} alt={articleObject?.title} />
                        </div>
                        <div dangerouslySetInnerHTML={{__html: articleObject?.body}} />
                    </div>
                    <div className={'right-section wowo animated fadeInUp'} style={{animationDelay: '0.2s'}}>
                        <h3>Featured Projects</h3>
                        <div className={'list'}>
                            {
                                this.state.featuredProjects?.map(
                                    (item, index) => (
                                        <div key={index} className="item">
                                            <Link to={`/projects/${item?.slug}/`}>
                                                <div className="image">
                                                    <img src={item?.image} alt={item?.name} />
                                                        <div className="hover">
                                                            <span className={'clear-right'}>View details</span>
                                                        </div>
                                                </div>
                                            </Link>
                                            <div className="title-section">
                                                <Link to={`/projects/${item?.slug}/`}>{item?.name}</Link>
                                            </div>
                                        </div>
                                    )
                                )
                            }
                        </div>
                        <div className={'buttons'}>
                            <span className={'clear-right'} onClick={this.handleLoginClick}>Login/Register</span>
                            <Link className={'clear-right'} to={'/contact-us'}>Contact Us</Link>
                        </div>
                    </div>
                </div>
                <NewsSectionComponent type={this.props.objectType || 'media'}/>
                <PageFooterComponent styles={{background: `rgba(232, 232, 232, 0.4) url(${footerBitmap}) no-repeat`}} />
            </div>
        );
    }
}
