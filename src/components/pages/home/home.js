import React, {Component} from "react";
import {withRouter} from "react-router";
import "./home.scss";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import footerBitmap from '../../../assets/footer-bitmap.png';
import homeMock from "./mock";
import ServiceSectionComponent from "./service-section";
import FeaturedProjectsSectionComponent from "./featured-projects-section";
import NewsSectionComponent from "./news-section";
import PageFooterComponent from "../../page-footer/page-footer";
import {Helmet} from "react-helmet";
import axiosService from "../../../services/axios.service";
import {Link} from "react-router-dom";
import PageLoadingComponent from "../../page-loading/page-loading";

class HomeComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            featuredProjects: undefined,
            services: undefined,
            news: undefined,
            pageObject: undefined
        }
    }

    componentDidMount() {
        this.setState({
            featuredProjects: homeMock.featuredProjects,
            services: homeMock.services,
            news: homeMock.news
        });

        axiosService().get('pages/home/').then(
            res => {
                if (res.status === 200) {
                    this.setState({
                        ...this.state,
                        pageObject: res.data
                    });
                }
            }
        );

        const videoEls = document.querySelectorAll('.video');
        for (const i of videoEls) {
            i.addEventListener('canplaythrough', ev => i.play());
        }
    }

    render() {
        const {pageObject} = this.state;
        return !pageObject ? <PageLoadingComponent /> : (
            <div className={'home'}>
                <Helmet><title>Home</title></Helmet>
                <Slider
                    settings={{
                        dots: false,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        speed: 500,
                        fade: true,
                        cssEase: 'linear',
                    }}
                >
                    {
                        pageObject?.meta_values?.slides?.map(
                            (item, index) => (
                                <div key={index}>
                                    <div
                                        className={'slide-page'}
                                        style={{
                                            backgroundImage: `url('${item?.image}')`,
                                            backgroundSize: 'cover',
                                            backgroundRepeat: 'no-repeat',
                                            backgroundPosition: '30px',
                                            width: '100%',
                                            display: 'inline-block'
                                        }}
                                    >
                                        {
                                            item.video ?
                                                <video className={'video'} loop={"loop"} muted autoPlay={"autoplay"}>
                                                    <source src={item.video} type="video/ogg" />
                                                </video>:
                                                undefined
                                        }
                                        <div className="text-block">
                                            <h1 className="wowo fadeInLeft animated" style={item?.title?.style}>{item?.title?.value}</h1>
                                            <div className="text-btn wowo fadeInLeft animated" style={{animationDelay: "0.4s"}}>
                                                <p style={item?.subtitle?.style}>{item?.subtitle?.value}</p>
                                                <Link className="btn gotonext clear-right" to={item?.button?.link} tabIndex="0">{item?.button?.text}</Link>
                                            </div>
                                        </div>
                                        <div className="hover" />
                                    </div>
                                </div>
                            )
                        )
                    }
                </Slider>
                <FeaturedProjectsSectionComponent />
                <ServiceSectionComponent data={pageObject?.meta_values?.services} />
                <NewsSectionComponent data={this.state.news} />
                <PageFooterComponent styles={{background: `rgba(232, 232, 232, 0.4) url(${footerBitmap}) no-repeat`}} />
            </div>
        );
    }
}

export default withRouter(HomeComponent);
