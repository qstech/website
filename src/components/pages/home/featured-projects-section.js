import React, {Component} from "react";
import {Link} from "react-router-dom";
import "./featured-projects-section.scss";
import axiosService from "../../../services/axios.service";
import PageLoadingComponent from "../../page-loading/page-loading";

export default class FeaturedProjectsSectionComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            projects: undefined
        }
    }
    componentDidMount() {
        axiosService().post(
            `projects/`,
            {
                home: true,
                page: 1,
                items_per_page: 3
            }
        ).then(
            res => {
                if (res.status === 200) {
                    this.setState({
                        ...this.state,
                        projects: res.data
                    });
                }
            }
        )
    }

    render() {
        const {projects} = this.state;
        return !projects ? <PageLoadingComponent /> : (
            <section className={'projects'}>
                <Link to={'/projects'}>
                    <div className={'projects-count'}>
                        <div className="projects-count-wrap">
                            <div className="projects-count-wrap-center">
                                <div className="count">{projects?.total}</div>
                                <p>Current projects</p>
                            </div>
                        </div>
                    </div>
                </Link>
                <div className="title wowo fadeInUp animated title-bar">
                    <div className="left">
                        <h2>Featured Project</h2>
                    </div>
                    <div className="right">
                        <Link to={'/projects'}>View all properties<span /></Link>
                    </div>
                </div>
                <div className={'grid'}>
                    {
                        projects?.items?.map(
                            (item, index) => (
                                <div key={index} className={'item wowo fadeInUp animated'} style={{animationDelay: `${ 0.2 * (index+1)}s`}}>
                                    <Link to={`/projects/${item.slug}/`} className={'open-modal'}>
                                        <div className={'image'}>
                                            <img src={item.image} alt={item.title} />
                                            <div className={'hover'}>
                                                <span className={'clear-right'}>View details</span>
                                            </div>
                                        </div>
                                    </Link>
                                    <div className={'contents'}>
                                        <div className={'title'}>
                                            <Link to={`/projects/${item.slug}/`}>{item.name}</Link>
                                            <p>{item.suburb}</p>
                                        </div>
                                    </div>
                                </div>
                            )
                        )
                    }
                </div>
            </section>
        );
    }
}
