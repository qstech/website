const homeMock = {
    news: {
        marketInsights: {
            description: '',
            articles: [
                {
                    key: 'which-suburbs-are-investable-in-2017',
                    image: 'https://commonrealty.com.au/wp-content/uploads/2018/07/home-marketinsight-2.jpg',
                    title: 'Which suburbs are investable in 2017?',
                    slug: 'which-suburbs-are-investable-in-2017'
                },
                {
                    key: 'property-market-strengths-across-australia',
                    image: 'https://commonrealty.com.au/wp-content/uploads/2018/07/home-marketinsight-1.jpg',
                    title: 'Property market strengths across Australia',
                    slug: 'property-market-strengths-across-australia',
                }
            ]
        },
        media: {
            articles: [
                {
                    key: 'nsw-off-the-plan-buyers-to-get-better-protection-under-new-laws-this-year',
                    title: 'NSW off-the-plan buyers to get better protection under new laws this year',
                    slug: 'nsw-off-the-plan-buyers-to-get-better-protection-under-new-laws-this-year',
                    image: 'https://commonrealty.com.au/wp-content/uploads/2019/09/Aspira-286x154.png'
                },
                {
                    key: 'reserve-bank-interest-rate-cut-likely-to-bolster-australias-property-market',
                    title: 'Reserve Bank interest rate cut likely to bolster Australia’s property market',
                    slug: 'reserve-bank-interest-rate-cut-likely-to-bolster-australias-property-market',
                    image: 'https://commonrealty.com.au/wp-content/uploads/2019/07/reserve-bank-286x154.jpg'
                },
                {
                    key: 'paying-off-a-mortgage-has-become-easier-in-most-capital-cities',
                    title: 'Paying off a mortgage has become easier in most capital cities',
                    slug: 'paying-off-a-mortgage-has-become-easier-in-most-capital-cities',
                    image: 'https://commonrealty.com.au/wp-content/uploads/2019/09/Aspira-286x154.png'
                },
                {
                    key: 'paying-off-a-mortgage-has-become-easier-in-most-capital-cities',
                    title: 'Housing Affordability The Best Since 2016, New ANZ-CoreLogic Report Finds',
                    slug: 'housing-affordability-the-best-since-2016-new-anz-corelogic-report-finds',
                    image: 'https://commonrealty.com.au/wp-content/uploads/2019/07/reserve-bank-286x154.jpg'
                },
            ]
        }
    }
}

export default homeMock;
