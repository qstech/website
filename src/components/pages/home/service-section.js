import React, {Component} from "react";
import "./service-section.scss";

export default class ServiceSectionComponent extends Component {
    render() {
        return (
            <section className={'service'}>
                <div className="left wowo fadeInUp animated" style={{animationDelay: "0.2s"}}>
                    <div className="icons">
                        {
                            this.props.data?.icons?.map(
                                (item, index) => (
                                    <div key={index} className={'item'}>
                                        <div className={'icon'}>
                                            <img src={item.icon} alt={item.text} />
                                        </div>
                                        <div className='text'>
                                            {item.text}
                                        </div>
                                    </div>
                                )
                            )
                        }
                    </div>
                </div>
                <div className="right wowo fadeInUp animated" style={{animationDelay: "0.4s"}}
                     dangerouslySetInnerHTML={{__html: this.props.data?.text}}
                />
            </section>
        );
    }
}
