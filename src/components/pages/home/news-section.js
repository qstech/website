import React, {Component} from "react";
import "./news-section.scss";
import {Link} from "react-router-dom";
import axiosService from "../../../services/axios.service";

export default class NewsSectionComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            media: [],
            marketInsight: []
        }
    }
    componentDidMount() {
        axiosService().post('articles/', {page: 1, items_per_page: 2, type: 'market-insight'}).then(
            res => {
                if (res.status === 200) {
                    this.setState({
                        marketInsight: res.data.items
                    })
                }
            }
        );
        axiosService().post('articles/', {page: 1, items_per_page: 4}).then(
            res => {
                if (res.status === 200) {
                    this.setState({
                        media: res.data.items
                    })
                }
            }
        );
    }

    render() {
        return (
            <section className={'news'}>
                {/*<section className={'market-insights'}>*/}
                {/*    <div className="title wowo fadeInUp animated title-bar">*/}
                {/*        <div className="left">*/}
                {/*            <h2>Market Research</h2>*/}
                {/*        </div>*/}
                {/*        <div className="right">*/}
                {/*            <Link to={'/market-insight/?tag=expert-advice'}>*/}
                {/*                Expert Advice*/}
                {/*                <span className="arrow-right" />*/}
                {/*            </Link>*/}
                {/*            <Link to={'/market-insight/?tag=investor-buyers-guide'}>*/}
                {/*                Investor &amp; Buyers Guide*/}
                {/*                <span className="arrow-right" />*/}
                {/*            </Link>*/}
                {/*        </div>*/}
                {/*    </div>*/}
                {/*    <p className="wowo fadeInUp animated">*/}
                {/*        Common Realty offers valuable information and constant market updates*/}
                {/*        for our clients who are interested in buying off-the-plan property in Sydney.*/}
                {/*        We hope that the research section could help our clients in making better, informed decisions.*/}
                {/*    </p>*/}
                {/*    <div className={'grid'}>*/}
                {/*        {*/}
                {/*            this.state.marketInsight?.map(*/}
                {/*                (item, index) => (*/}
                {/*                    <div key={index} className="item wowo fadeInUp animated"*/}
                {/*                         style={{animationDelay: `${0.2 * (index + 1)}s`}}*/}
                {/*                    >*/}
                {/*                        <Link to={`/market-insight/${item.slug}/`}>*/}
                {/*                            <div className="image">*/}
                {/*                                <img src={item.image} alt={item.title} />*/}
                {/*                                <div className="hover">*/}
                {/*                                    <span className={'clear-right'}>Read more</span>*/}
                {/*                                </div>*/}
                {/*                            </div>*/}
                {/*                        </Link>*/}
                {/*                        <div className={'contents'}>*/}
                {/*                            <div className="title">*/}
                {/*                                <Link to={`/market-insight/${item.slug}/`}>{item.title}</Link>*/}
                {/*                            </div>*/}
                {/*                        </div>*/}
                {/*                    </div>*/}
                {/*                )*/}
                {/*            )*/}
                {/*        }*/}
                {/*    </div>*/}
                {/*</section>*/}
                <section className={'media fixed-width-container'}>
                    <div className="title wowo fadeInUp animated">
                        <div className="left">
                            <h2>Media</h2>
                        </div>
                    </div>
                    <div className={'grid'}>
                        {
                            this.state.media?.map(
                                (item, index) => (
                                    <div key={index} className="item wowo fadeInUp animated"
                                         style={{animationDelay: `${0.2 * (index + 1)}s`}}
                                    >
                                        <Link to={`/media/${item.slug}/`}>
                                            <div className="image">
                                                <img src={item.image} alt={item.title} />
                                                <div className="hover">
                                                    <span className={'clear-right'}>Read more</span>
                                                </div>
                                            </div>
                                        </Link>
                                        <div className="title">
                                            <Link to={`/media/${item.slug}/`}>{item.title}</Link>
                                        </div>
                                    </div>
                                )
                            )
                        }
                    </div>
                </section>
            </section>
        );
    }
}
