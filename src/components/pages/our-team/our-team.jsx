import {withRouter} from "react-router";
import PagePaddingComponent from "../../page-padding/page-padding";
import {Helmet} from "react-helmet";
import axiosService from "../../../services/axios.service";
import PageLoadingComponent from "../../page-loading/page-loading";
import PageFooterComponent from "../../page-footer/page-footer";
import {Card} from "antd";
import './our-team.scss';
import Modals from "../../../layouts/modals";
import TeamMemberDetailsModal from "../../modals/team-member-details";

const {Component} = require("react");

class OurTeamPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            teamMembers: [],
            teamMemberDesc: '',
            descLoading: true,
            listLoading: true,
            notFound: false
        }
    }

    componentDidMount() {
        axiosService().post('pages/',
            {
                scopes: ['team']
            }
        ).then(
            res => {
                this.setState({
                    descLoading: false
                })
                if (res.status === 200) {
                    this.setState({
                        teamMemberDesc: res.data.data.team_page_description.value
                    })
                } else {
                }
            }
        );

        axiosService().get('team/members/').then(
            res => {
                console.log(res.data)
                this.setState({
                    listLoading: false
                })
                if (res.status === 200) {
                    this.setState({
                        teamMembers: res.data.data
                    })
                } else {
                    this.setState({
                        teamMembers: [],
                        notFound: true
                    });
                }
            }
        )
    }

    handleTeamMemberClick = (e, teamMemberObj) => {
        e.preventDefault();
        Modals.create(<TeamMemberDetailsModal teamMemberObj={teamMemberObj} />);
    }

    render() {
        return (
            (!this.state.descLoading && !this.state.listLoading) ?
            <>
                <div className='our-team-page-container'>
                    <PagePaddingComponent />
                    <Helmet><title>Our Team</title></Helmet>
                    <div className={'our-team-list-section fixed-width-container wrap'}>
                        <div className="title wowo fadeInUp animated">
                            <div className="left">
                                <h1>Our Team</h1>
                            </div>
                        </div>
                        <div className='team-description wowo fadeInUp animated' dangerouslySetInnerHTML={{__html: this.state.teamMemberDesc}} />
                        <div className={'list'}>
                            {
                                this.state.notFound ? <h2 className="no-data wowo animated fadeInUp">No Team Member has been found.</h2> : undefined
                            }
                            {
                                this.state.teamMembers?.map(
                                    (item, index) => (
                                        <Card hoverable key={index} className="item wowo fadeInUp animated"
                                              style={{animationDelay: `${0.2 * (index + 1)}s`}}
                                              title={null}
                                              bodyStyle={{padding: 0}}
                                        >
                                            <a href='#' onClick={(e) => this.handleTeamMemberClick(e, item)}>
                                                <div className={'image'}>
                                                    <img src={item?.assets[0]?.file} alt={`${item?.first_name} ${item?.last_name}`} />
                                                    <img src={item?.assets[1]?.file} alt='' />
                                                    {/*<div className="hover">*/}
                                                    {/*    <span className={'clear-right'}>Read more</span>*/}
                                                    {/*</div>*/}
                                                </div>
                                            </a>
                                            <div className="title">
                                                <a href='#' onClick={(e) => this.handleTeamMemberClick(e, item)}>{`${item?.first_name} ${item?.last_name}`}</a>
                                                <p>{item?.title}</p>
                                            </div>
                                        </Card>
                                    )
                                )
                            }
                        </div>
                    </div>
                </div>
                <PageFooterComponent />
            </>
            :
            <PageLoadingComponent />
        )
    }
}

export default withRouter(OurTeamPage);
