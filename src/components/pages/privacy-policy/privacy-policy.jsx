import {Component} from "react";
import PagePaddingComponent from "../../page-padding/page-padding";
import {Helmet} from "react-helmet";
import axiosService from "../../../services/axios.service";
import {withRouter} from "react-router";

class PrivacyPolicy extends Component {
    constructor(m) {
        super(m);
        this.state = {
            page_info: ''
        }
    }
    componentDidMount() {
        axiosService().post('pages/', {scopes: ['disclaimer']}).then(
            res => {
                console.log(res)
                this.setState({page_info: res.data.data})
            }
        )
    }

    render() {
        return (
            <div className='our-team-page-container'>>
                <PagePaddingComponent />
                <Helmet><title>Privacy Policy</title></Helmet>
                <div className={'our-team-list-section fixed-width-container wrap'}>
                    <div className="title wowo fadeInUp animated">
                        <div className="left">
                            <h1>Privacy Policy</h1>
                        </div>
                    </div>
                    <div dangerouslySetInnerHTML={{__html: this.state.page_info.privacy_policy?.value}} />
                </div>
            </div>
        )
    }
}

export default withRouter(PrivacyPolicy);
