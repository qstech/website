import React, {Component} from "react";
import {withRouter} from "react-router";
import "./contact-us.scss";
import {Form, message} from "antd";
import GoogleMapReact from "google-map-react";
import config from "../../../common/config";
import googleMapMarker from "../../../assets/icons/google-map-marker.svg";
import {Helmet} from "react-helmet";
import PagePaddingComponent from "../../page-padding/page-padding";
import axiosService from "../../../services/axios.service";
import {GoogleMap, LoadScript, Marker} from '@react-google-maps/api'

class ContactUs extends Component {
    googleMapPageUrl = 'https://www.google.com.au/maps/place/shop+8%2F3-9+Spring+St,+Chatswood+NSW+2067/' +
        '@-33.7967241,151.1829683,17z'

    constructor(props) {
        super(props);
        this.state = {
            contactObj: null
        }
    }

    componentDidMount() {
        axiosService().post('pages/', {scopes: ['contact']}).then(
            res => {
                console.log(res.data.data)
                this.setState({contactObj: res.data.data})
            }
        )
    }

    handleSubmit = (values) => {
        axiosService().post('forms/', values).then(
            res => {
                if (res.status === 201) {
                    message.success({
                        content: 'Form successfully submitted. Thank you for your enquiry.',
                        onClose: () => window.location.reload()
                    })
                }
            }
        )
    }

    render() {
        return (
            <div className={'contact-us-container'}>
                <Helmet><title>Contact Us</title></Helmet>
                <div className={'contact-us-wrap fixed-width-container'}>
                    <PagePaddingComponent />
                    <div className={'title'}>
                        <div className={'wowo fadeInUp animated'}>
                            <h1>Contact Us</h1>
                        </div>
                    </div>
                    <div className={'form'}>
                        <div className="left wowo fadeInUp animated" style={{animationDelay: "0.2s"}}>
                            <div className="text-block office">
                                <h2>Head Office</h2>
                                <p>
                                    <strong>Address</strong>
                                    <a
                                        href={this.googleMapPageUrl}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                    >
                                        Shop 8, 3-9 Spring Street, Chatswood NSW 2067
                                    </a>
                                </p>
                                <p>
                                    <strong>Phone</strong>
                                    <a href="tel:61294381451">+61 2 9438 1451</a>
                                </p>
                                <p>
                                    <strong>Email</strong>
                                    <a
                                        href="mailto:info@commonrealty.com.au"
                                    >
                                        info@commonrealty.com.au
                                    </a>
                                </p>
                            </div>
                            <div className="text-block hours">
                                <h2>Trading Hours</h2>
                                <p><strong>Monday to Friday</strong> 10:00am to 5:00pm</p>
                                <p><strong>Saturday and Sunday</strong> By Appointment</p>
                            </div>
                        </div>
                        <div className={'right wowo animated fadeInUp'} style={{animationDelay: '0.4s'}}>
                            <h2>General Enquiry, How Can We Help?</h2>
                            <Form
                                name={'contact-form'}
                                initialValues={
                                    {
                                        firstName: undefined,
                                        lastName: undefined,
                                        email: undefined,
                                        phone: undefined,
                                        message: undefined,
                                        type: 'general'
                                    }
                                }
                                onFinish={this.handleSubmit}
                            >
                                <Form.Item name={'type'} hidden>
                                    <input type={'hidden'} hidden />
                                </Form.Item>
                                <div className={'line'}>
                                    <Form.Item
                                        name={'firstName'}
                                        rules={[{required: true, message: 'First Name is required'}]}
                                    >
                                    <span>
                                        <input type={'text'} placeholder={'First Name'} />
                                    </span>
                                    </Form.Item>
                                    <Form.Item
                                        name={'lastName'}
                                        rules={[{required: true, message: 'Last Name is required'}]}
                                    >
                                    <span>
                                        <input type={'text'} placeholder={'Last Name'} />
                                    </span>
                                    </Form.Item>
                                </div>
                                <div className={'line'}>
                                    <Form.Item
                                        name={'email'}
                                        rules={[{required: true, message: 'Email is required'}]}
                                    >
                                        <input type={'email'} placeholder={'Email'} />
                                    </Form.Item>
                                    <Form.Item
                                        name={'phone'}
                                    >
                                        <input type={'phone'} placeholder={'Phone'} />
                                    </Form.Item>
                                </div>
                                <div className={'message-line line'}>
                                    <Form.Item
                                        name={'message'}
                                        rules={[{required: true, message: 'Message is required'}]}
                                    >
                                        <textarea cols={40} rows={10} placeholder={'Your Message'} />
                                    </Form.Item>
                                </div>
                                <div className={'submit-line line'}>
                                    <Form.Item>
                                        <input type={'submit'} value={'Submit'} />
                                    </Form.Item>
                                </div>
                            </Form>
                        </div>
                    </div>
                </div>
                <div className={'map-wrap'}>
                    <LoadScript googleMapsApiKey={config.googleMapKey}>
                        <GoogleMap
                            mapContainerStyle={{width: '100%', height: '100%'}}
                            zoom={16}
                            center={{lat: -33.7967241, lng: 151.185157}}
                            options={{styles: config.googleMapStyleContact, scrollwheel: true, draggable: false}}
                        >
                            <Marker position={this.state?.contactObj?.office_address?.value?.coordinates}
                                    icon={googleMapMarker}
                                    onClick={() => {
                                        window.open(
                                            'https://www.google.com.au/maps/place/' +
                                            this.state?.contactObj?.office_address?.value?.address?.replaceAll(' ', '+').replaceAll('/', '+') +
                                            `/@${this.state?.contactObj?.office_address?.value?.coordinates?.lat},${this.state?.contactObj?.office_address?.value?.coordinates?.lng},17z`,
                                            '_blank'
                                        )
                                    }}
                            />
                        </GoogleMap>
                    </LoadScript>
                </div>
            </div>
        )
    }
}

export default withRouter(ContactUs);
