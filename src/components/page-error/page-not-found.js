import React, {Component} from "react";
import "./page-not-found.scss";
import PagePaddingComponent from "../page-padding/page-padding";
import PageFooterComponent from "../page-footer/page-footer";
import {Helmet} from "react-helmet";

export default class PageNotFound extends Component {
    render() {
        return (
            <div>
                <Helmet><title>Page Not Found</title></Helmet>
                <div className={'page-not-found-container fixed-width-container'}>
                    <PagePaddingComponent />
                    <div className={'title'}>
                        <div className={'wowo fadeInUp animated'}>
                            <h1>404 not found</h1>
                        </div>
                    </div>
                    <div className={'text-box wowo fadeInUp animated'}>
                        <h2>Sorry, nothing here!</h2>
                    </div>
                </div>
                <PageFooterComponent />
            </div>
        );
    }
}
