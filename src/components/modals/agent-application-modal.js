import React, {useEffect, useState} from "react";
import {Form, Result} from "antd";
import industryData from '../../assets/industries.json';
import Modals from "../../layouts/modals";
import axiosService from "../../services/axios.service";
import {CheckCircleOutlined} from '@ant-design/icons';
import LoginModal from "./login-modal";
import AuthService from "../../services/auth.service";

export default function AgentApplicationModal(props) {
    const [form] = Form.useForm();

    useEffect(() => {
    });

    let user = props.userObject;
    const [result, setResult] = useState(null);

    const handleSubmit = (values) => {
        if (user) {
            values.email = null;
            values.password = null;
            values.password_confirm = null;
            values.name = null;
        }
        axiosService({attachAuth: true}).post('forms/', values).then(
            res => {
                if (res?.status === 201) {
                    setResult('success');
                } else {
                    setResult('warning');
                }
            }
        )
    }

    return (
        <div className={'agent-application-modal-container'}>
            {
                result ? <Result
                        status={result}
                        icon={result === 'success' ? <CheckCircleOutlined style={{color: '#00ae9e'}}/> : null}
                        title={result === 'success' ? "Congratulations!" : "Application Failed"}
                        subTitle={
                            result === 'success' ?
                                "Your application has been successfully sent." :
                                "Unfortunately, your application is not successfully sent, please try again. if you already have a account with us, please log in."}
                        extra={
                            result === 'success' ?
                                <input type={"submit"} value={'OK'}
                                       onClick={() => {
                                           if (user) {
                                               Modals.destroy();
                                           } else {
                                               AuthService.login(form.getFieldValue('email'), form.getFieldValue('password')).then(
                                                   res => {
                                                       if (res.status === 200) {
                                                           AuthService.saveToken(res.data);
                                                       }
                                                       window.location.reload();
                                                   }
                                               )
                                           }

                                       }} /> :
                                [
                                    <input type={'submit'} value={'Go back'} onClick={() => setResult(null)}/>,
                                    <input type={'submit'} value={'Login'} onClick={() => Modals.create(<LoginModal/>)}/>,
                                ]
                        }
                    /> :
                    <Form
                        name={'agent-application-form'}
                        initialValues={
                            {
                                type: 'agent-application',
                                company: '',
                                role: '',
                                industry: '',
                                phone: user?.phone || '',
                                name: user?.full_name || '',
                                email: user?.username || '',
                                password: user ? '**********' : '',
                                password_confirm: user ? '**********' : ''
                            }
                        }
                        form={form}
                        onFinish={handleSubmit}
                    >
                        <div className={'left'}>
                            <h4>Agent Application</h4>
                            <p>Early access to the best Sydney projects, market updates & information</p>
                        </div>
                        <div className={'middle'}>
                            <Form.Item name={'type'} hidden>
                                <input type={"hidden"}/>
                            </Form.Item>
                            <Form.Item
                                name={'company'}
                                hasFeedback
                                rules={[{
                                    required: true,
                                    message: 'Company is required, if you are acting on individual, please use your full name.'
                                }]}
                            >
                                <input type={"text"} placeholder={"Company *"} required/>
                            </Form.Item>
                            <Form.Item
                                name={'role'}
                                rules={[{required: true, message: 'Role in company is required.'}]}
                            >
                                <input type={"text"} placeholder={"Role *"} required/>
                            </Form.Item>
                            <Form.Item
                                name={'industry'}
                                rules={[{
                                    required: true,
                                    message: 'Company is required, if you are acting on individual, please use your full name.'
                                }]}
                            >
                                <select>
                                    <option value={''}>Select an industry *</option>
                                    {
                                        industryData?.map(
                                            (item, index) => (
                                                <option value={item} key={index}>{item}</option>
                                            )
                                        )
                                    }
                                </select>
                            </Form.Item>
                            <Form.Item
                                name={'phone'}
                                rules={[{required: true, message: 'Phone number is required'}]}
                            >
                                <input type={"tel"} placeholder={"Phone *"} required/>
                            </Form.Item>
                        </div>
                        <div className={'right'}>
                            <div className={`disable-cover ${user ? 'active' : ''}`}>
                                <h4>Logged in as {user?.full_name}</h4>
                            </div>
                            <Form.Item
                                name={'name'}
                                rules={[{required: true, message: 'Name is required.'}]}
                            >
                                <input type={"text"} placeholder={"Name *"} required disabled={user}/>
                            </Form.Item>
                            <Form.Item
                                name={'email'}
                                rules={[{required: true, message: 'Email is required.'}]}
                            >
                                <input type={"email"} placeholder={"Email *"} required disabled={user}/>
                            </Form.Item>
                            <Form.Item
                                name={'password'}
                                rules={
                                    [
                                        {required: true, message: 'Password is required.'},
                                        {min: 8, message: 'Minimum password length is 8'},
                                        {
                                            validator: (rule, value) => {
                                                const passwordRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})");
                                                if (value && !passwordRegex.test(value) && !user) {
                                                    return Promise.reject('The password must contain at least 1 uppercase letter, 1 lowercase letter and 1 number.');
                                                } else {
                                                    return Promise.resolve();
                                                }
                                            }
                                        }
                                    ]
                                }
                            >
                                <input type='password' autoComplete='new-password' placeholder='Password *' required
                                       disabled={user}/>
                            </Form.Item>
                            <Form.Item
                                name={'password_confirm'}
                                rules={
                                    [
                                        {required: true, message: 'Please enter your password again.'},
                                        {
                                            validator: (rule, value) => {
                                                if (value && value !== form.getFieldValue('password')) {
                                                    return Promise.reject('Passwords do not match');
                                                } else {
                                                    return Promise.resolve();
                                                }
                                            }
                                        }
                                    ]
                                }
                            >
                                <input type='password' autoComplete='new-password' placeholder='Confirm password *'
                                       required disabled={user}/>
                            </Form.Item>
                            <Form.Item>
                                <input type={'submit'} value={'Apply'}/>
                            </Form.Item>
                        </div>
                        <div className="close" onClick={() => Modals.destroy()}/>
                    </Form>
            }
        </div>
    );
}
