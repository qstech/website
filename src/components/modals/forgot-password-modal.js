import React, {Component} from "react";
import {Form} from "antd";
import close from "../../assets/icons/close.svg";
import Modals from "../../layouts/modals";
import LoginModal from "./login-modal";

export default class ForgotPasswordModal extends Component {

    onSubmit = (values) => {
        console.log(values)
    }

    handleSignIn(ev) {
        ev.preventDefault();
        Modals.create(<LoginModal />);
    }

    render() {
        const {Item} = Form;
        return (
            <div className={'forgot modal-container'}>
                <div className={'left'}>
                    <h4>FORGOT PASSWORD</h4>
                    <h2>Please enter your username or email address. You will receive a link to create a new password via email.</h2>
                </div>
                <div className={'right'}>
                    <Form
                        name={'forgot-form'}
                        initialValues={{
                            email: '',
                        }}
                        onFinish={this.onSubmit}
                    >
                        <Item name={'email'}>
                            <input type={'email'} autoComplete={'username'} className={'input'} placeholder={'Username or Email'} />
                        </Item>
                        <Item>
                            <input type='submit' value={'Get New Password'} />
                        </Item>
                    </Form>
                    <div className={'actions'}>
                        <div>
                            <p>
                                Return to <a href={'#'} onClick={this.handleSignIn}>Sign In</a> Page
                            </p>
                        </div>
                    </div>
                </div>
                <div className={'close'} onClick={Modals.destroy} />
            </div>
        );
    }
}
