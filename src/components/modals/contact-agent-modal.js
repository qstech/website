import React, {Component} from "react";
import {Checkbox, Form, message} from "antd";
import close from "../../assets/icons/close.svg";
import Modals from "../../layouts/modals";
import axiosService from "../../services/axios.service";
import modals from "../../layouts/modals";
import ReactGA from "react-ga";

export default class ContactAgentModal extends Component {

    handleContactUsSubmit = (values) => {
        ReactGA.event({
            category: 'tracking',
            action: 'contact_project',
            label: 'contact_project_tag',
        });
        axiosService().post('forms/', values).then(
            res => {
                if (res.status === 201) {
                    message.success({
                        content: 'Form successfully submitted. Thank you for your enquiry.',
                        onClose: () => modals.destroy()
                    })
                }
            }
        )
    }

    render() {
        return (
            <div className={'contact-agent-modal'}>
                <div>
                    <div className="title-wrap">
                        <h2>Got questions for this property?</h2>
                        <h4>Leave us a message and we will get back to you as soon as possible.</h4>
                    </div>
                    <hr />
                    <Form
                        initialValues={{type: 'project', project: this.props.project || null}}
                        className={'contact-agent-form'}
                        onFinish={this.handleContactUsSubmit}
                    >
                        <Form.Item name={'type'} hidden>
                            <input type={'hidden'} hidden />
                        </Form.Item>
                        <Form.Item name={'project'} hidden>
                            <input type={'hidden'} hidden />
                        </Form.Item>
                        <div className={'top-section'}>
                            <h4 className={'modal-section-title'}>My Enquiry</h4>
                            <Form.Item name={'enquiry'} rules={[{required: true, message: 'Enquiry is required'}]}>
                                <textarea rows={8} placeholder={'Tell us more so that we can better assist you.'} />
                            </Form.Item>
                        </div>
                        <div className={'bottom-section'}>
                            <div className={'left-wrap'}>
                                <h4 className="modal-section-title">Please send me more information</h4>
                                <Form.Item name={'requested'}>
                                    <Checkbox.Group className="options">
                                        <Checkbox value={'brochure'}>Brochure</Checkbox>
                                        <Checkbox value={'floor_plan'}>Floor Plan</Checkbox>
                                        <Checkbox value={'pricing'}>Pricing</Checkbox>
                                        <Checkbox value={'similar_projects'}>Similar Projects</Checkbox>
                                    </Checkbox.Group>
                                </Form.Item>
                            </div>
                            <div className="right-wrap">
                                <h4 className="modal-section-title">Contact Details</h4>
                                <Form.Item name={'name'} rules={[{required: true, message: 'Name is required'}]}>
                                    <input type="text" placeholder="Name" />
                                </Form.Item>
                                <Form.Item name={'phone'} rules={[{required: true, message: 'Phone is required'}]}>
                                    <input type="text" placeholder="Phone" />
                                </Form.Item>
                                <Form.Item name={'email'} rules={[{required: true, message: 'Email is required'}]}>
                                    <input type="email" placeholder="Email" />
                                </Form.Item>
                                <Form.Item name={'whatsapp'}>
                                    <input type="text" placeholder={'Whatsapp'} />
                                </Form.Item>
                                <Form.Item name={'wechat'}>
                                    <input type="text" placeholder={'WeChat'} />
                                </Form.Item>
                            </div>
                        </div>
                        <div className={'submit'}>
                            <Form.Item>
                                <input className={'submit'} type="submit" value="Submit" />
                            </Form.Item>
                        </div>
                    </Form>

                </div>
                <div className={'close'} onClick={Modals.destroy} />
            </div>
        );
    }
}
