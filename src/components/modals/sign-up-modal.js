import React, {Component} from "react";
import {Button, Form, message} from "antd";
import close from "../../assets/icons/close.svg";
import Modals from "../../layouts/modals";
import LoginModal from "./login-modal";
import ForgotPasswordModal from "./forgot-password-modal";
import AuthService from "../../services/auth.service";
import ReactGA from 'react-ga';

export default class SignUpModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false
        }
    }

    onSubmit = (values) => {
        this.setState({loading: true})
        ReactGA.event({
            category: 'tracking',
            action: 'sign_up',
            label: 'sign_up_tag',
        });
        AuthService.register(values).then(
            res => {
                if (res.status === 201) {
                    AuthService.login(values.email, values.password).then(
                        response => {
                            if (response.status === 200) {
                                AuthService.saveToken(response.data);
                                message.success('You have successfully registered, login now').then(
                                    res => {
                                        this.setState({loading: false});
                                        window.location.reload();
                                    }
                                )

                            }
                        }
                    )
                } else {
                    this.setState({loading: false});
                    message.error('Error registering, please try again')
                }
            }
        )
    }

    handleLoginClicked = (ev) => {
        ev.preventDefault();
        Modals.create(<LoginModal />);
    }

    handleForgotPassword = (ev) => {
        ev.preventDefault();
        Modals.create(<ForgotPasswordModal />);
    }

    render() {
        const {Item} = Form;
        return (
            <div className={'sign-up modal-container'}>
                <div className={'left'}>
                    <h4>SIGN UP</h4>
                    <h2>To explore our projects and price range, please sign up.</h2>
                </div>
                <div className={'right'}>
                    <Form
                        name={'login-form'}
                        initialValues={{
                            name: '',
                            email: '',
                            password: '',
                            confirm_password: '',
                        }}
                        onFinish={this.onSubmit}
                    >
                        <Item name={'first_name'}>
                            <input type={'text'} autoComplete={'firstName'} className={'input'} placeholder={'First Name*'} />
                        </Item>
                        <Item name={'last_name'}>
                            <input type={'text'} autoComplete={'lastName'} className={'input'} placeholder={'Last Name*'} />
                        </Item>
                        <Item name={'email'}>
                            <input type={'email'} autoComplete={'username'} className={'input'} placeholder={'Email*'} />
                        </Item>
                        <Item name={'phone'}>
                            <input type={'tel'} autoComplete={'phone'} className={'input'} placeholder={'Phone*'} />
                        </Item>
                        <Item name={'password'}>
                            <input type='password' autoComplete={'new-password'} className={'input'} placeholder={'Password*'} />
                        </Item>
                        <Item name={'confirm_password'}>
                            <input type='password' autoComplete={'new-password'} className={'input'} placeholder={'Confirm Password*'} />
                        </Item>
                        <Item>
                            <input className={this.state.loading && 'loading'} type='submit' value={'Sign Up'} />
                        </Item>
                    </Form>
                    <div className={'actions'}>
                        <div>
                            <p>
                                Don’t have an account? <a href={'#'} onClick={this.handleLoginClicked}>Sign In Now</a>
                            </p>
                        </div>
                        <div>
                            <p>
                                <a href={'#'} onClick={this.handleForgotPassword}>Forgot Password</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div className={'close'} onClick={Modals.destroy} />
            </div>
        );
    }
}
