import React, {Component} from "react";
import {Form, message} from "antd";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import facebookIcon from "../../assets/icons/facebook.svg";
import Modals from "../../layouts/modals";
import SignUpModal from "./sign-up-modal";
import ForgotPasswordModal from "./forgot-password-modal";
import AuthService from "../../services/auth.service";
import config from "../../common/config";

export default class LoginModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false
        }
    }

    onSubmit = (values) => {
        this.setState({loading: true})
        AuthService.login(values.email, values.password).then(
            res => {
                if (res.status === 200) {
                    AuthService.saveToken(res.data);
                    message.success('Login successful.').then(
                        () => {
                            this.setState({loading: false});
                            window.location.reload();
                        },
                        null
                    )
                } else {
                    this.setState({loading: false})
                    message.error('No email/password found matched, please try again.')
                }
            }
        )
    }

    handleFacebookLoginClick = (ev) => {
        ev.preventDefault();
        ev.stopPropagation();
        console.log(ev)
    }

    facebookLoginCallback = (res) => {
        console.log(res)
    }

    handleRegister(ev) {
        ev.preventDefault();
        Modals.create(<SignUpModal />);
    }

    handleForgotPassword(ev) {
        ev.preventDefault();
        Modals.create(<ForgotPasswordModal />);
    }

    render() {
        const {Item} = Form;
        return (
            <div className={'login modal-container'}>
                <div className={'left'}>
                    <h4>LOGIN</h4>
                    <h2>To explore our projects and price range, please log in.</h2>
                </div>
                <div className={'right'}>
                    <Form
                        name={'login-form'}
                        initialValues={{
                            email: '',
                            password: ''
                        }}
                        onFinish={this.onSubmit}
                    >
                        <Item name={'email'} rules={[{required: true, message: 'Email is required'}]}>
                            <input type={'email'} autoComplete={'username'} className={'input'} placeholder={'Email*'} />
                        </Item>
                        <Item name={'password'} rules={[{required: true, message: 'Password is required'}]}>
                            <input type='password' autoComplete={'current-password'} className={'input'} placeholder={'Password*'} />
                        </Item>
                        <Item>
                            <input className={this.state.loading && 'loading'} type='submit' value={'Login'} />
                        </Item>
                    </Form>
                    <div className={'actions'}>
                        <div>
                            <p>
                                Don’t have an account? <a href={'#'} onClick={this.handleRegister}>Register Now</a>
                            </p>
                        </div>
                        <div>
                            <p>
                                <a href={'#'} onClick={this.handleForgotPassword}>Forgot Password</a>
                            </p>
                        </div>
                    </div>
                    {/*TODO: fix facebook login*/}
                    {/*<FacebookLogin*/}
                    {/*    appId={config.facebookAppId}*/}
                    {/*    autoLoad={false}*/}
                    {/*    scope='public_profile, email'*/}
                    {/*    fields='name, email'*/}
                    {/*    callback={this.facebookLoginCallback}*/}
                    {/*    render={renderProps => (*/}
                    {/*        <a*/}
                    {/*            className={'facebook-login-btn'}*/}
                    {/*            onClick={renderProps.onClick}*/}
                    {/*        >*/}
                    {/*            Login with Facebook*/}
                    {/*        </a>*/}
                    {/*    )}*/}
                    {/*/>*/}
                </div>
                <div className={'close'} onClick={Modals.destroy} />
            </div>
        );
    }
}
