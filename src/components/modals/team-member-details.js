import React, {Component} from "react";
import Modals from "../../layouts/modals";
export default class TeamMemberDetailsModal extends Component {

    destroy = () => {
        if (window.innerWidth < 1024) {
            Modals.destroy();
        }
    }

    render() {
        const {teamMemberObj} = this.props;
        return (
            <div className='team-member-details-container modal-container' onClick={this.destroy}>
                <div className='img'>
                    {
                        teamMemberObj.assets?.map(
                            (item, index) => (
                                index > 1 ? <img style={{zIndex: index}} src={item.file} alt={`${teamMemberObj.first_name} ${teamMemberObj.last_name}`} /> : null
                            )
                        )
                    }
                </div>
                <div className='content-wrap'>
                    <h3>{`${teamMemberObj.first_name} ${teamMemberObj.last_name}`}</h3>
                    <strong className='title'>{teamMemberObj.title}</strong>
                    <a href={`tel:${teamMemberObj.phone}`}>{teamMemberObj.phone}</a>
                    <a href={`mailto:${teamMemberObj.email}`}>{teamMemberObj.email}</a>
                    <div className='desc' dangerouslySetInnerHTML={{__html: teamMemberObj.desc}} />
                    <strong>Strength</strong>
                    <ul>
                        {
                            teamMemberObj.strength?.split(',').map(
                                (item, index) => (
                                    <li key={index}>{item}</li>
                                )
                            )
                        }
                    </ul>
                </div>
            </div>
        )
    }
}


