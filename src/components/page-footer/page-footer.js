import React, {Component} from "react";
import './page-footer.scss';
import footerBitmap from '../../assets/footer-bitmap.png';

export default class PageFooterComponent extends Component {
    render() {
        return (
            <div
                className={'bg-img'}
                style={
                    {
                        background: `url('${footerBitmap}') no-repeat`,
                        ...this.props.styles
                    }
                }
            />
        );
    }
}
