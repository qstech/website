import React, {Component} from "react";
import "./page-padding.scss";

export default class PagePaddingComponent extends Component {
    render() {
        return (
            <div className={'page-padding'}>&nbsp;</div>
        );
    }
}
