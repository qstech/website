import React, {Component} from "react";
import {Spin} from "antd";
import "./page-loading.scss";
import {LoadingOutlined} from '@ant-design/icons';

class PageLoadingComponent extends Component {
    render() {
        return (
            <Spin className={'page-loader'} spinning={true} size={"large"} indicator={<LoadingOutlined style={{color: '#00ae9e', fontSize: '100px'}}/>}>
                <div className={'page-loading-container fixed-width-container'} />
            </Spin>
        );
    }
}

export default PageLoadingComponent;
