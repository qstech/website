import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {GoogleReCaptchaProvider} from "react-google-recaptcha-v3";
import config from "./common/config";

ReactDOM.render(
    <GoogleReCaptchaProvider
        useRecaptchaNet={true}
        reCaptchaKey={config.googleReCaptchaKey}
    >
        <App />
    </GoogleReCaptchaProvider>
    // <React.StrictMode>
    //     <App />
    // </React.StrictMode>
    , document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
