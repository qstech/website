import "./modals.scss";
import {Modal} from "antd";

export default class Modals {
    static create(modalTemplate) {
        Modals.destroy();
        Modal.success({
            title: null,
            content: modalTemplate,
            centered: true,
            width: 'auto',
            maskClosable: true,
            maskStyle: {background: 'rgba(0, 0, 0, 0.7)'}
            // modalRender: node => <Modals type={'login'} />
        });
    }

    static destroy() {
        Modal.destroyAll();
    }
}
