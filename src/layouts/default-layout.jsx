import React, {Component} from "react";
import "./default-layout.scss";
import {Redirect, withRouter} from "react-router";
import {Route, Switch} from "react-router-dom";
import PageLoadingComponent from "../components/page-loading/page-loading";
import PageNotFound from "../components/page-error/page-not-found";
import routes from "./routes";
import {Helmet} from "react-helmet";
import Utils from "./utils";

class DefaultLayout extends Component {
    componentDidMount() {
        if (this.props.onLoad) {
            this.props.onLoad();
        }
    }

    render() {
        return (
            <div className={`content-container`}>
                <Helmet titleTemplate={"%s - CommonRealty"} defaultTitle={'CommonRealty'} />
                    <React.Suspense fallback={<PageLoadingComponent/>}>
                        <Switch>
                            {
                                routes.map(
                                    (item) => {
                                        const Component = React.lazy(() => import(`../components/${item.component}`));
                                        return (
                                            <Route
                                                strict={item.strict}
                                                exact={item.exact}
                                                key={item.name}
                                                path={item.url}
                                                render={() => <Component
                                                    onLoad={item.animated ? Utils.animatedClassWhenInView() : undefined}/>}
                                            />
                                        );
                                    }
                                )
                            }
                            {/*<Route exact strict path={'/'} component={React.lazy(()=> import('../components/pages/home/home'))} />*/}
                            <Redirect exact path={'/'} to={'/home'}/>
                            <Route render={() => <PageNotFound/>}/>
                        </Switch>
                    </React.Suspense>
                <script>
                    window.onload = function () {
                        window.scrollTo({top: 0})
                    }
                </script>
            </div>
        );
    }
}

export default withRouter(DefaultLayout);
