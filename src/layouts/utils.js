export default class Utils {
    static numberIterator(min, max, step) {
        const output = [];
        for (let i = min; i <= max; i += step ) {
            output.push(i);
        }
        return output;
    }

    static currencyTransformer(value) {
        return new Intl.NumberFormat('en-AU', {
            style: 'currency',
            currency: 'AUD',
            currencyDisplay: 'symbol',
            minimumFractionDigits: 0
        }).format(value);
    }

    static convertTextToSlugFormat(text) {
        return text.replaceAll(' ', '-').toLowerCase();
    }

    static scrollToElementID(id) {
        if (!id) {
            window.scrollTo({behavior: "smooth", top: 0})
        } else {
            const el = document.getElementById(id);
            if (el) {
                console.log(el.getBoundingClientRect().top - 134)
                window.scrollTo({left: 0, top: el.getBoundingClientRect().top - 200 + window.pageYOffset, behavior: "smooth"});
            }
        }
    }

    static animatedClassWhenInView() {
        let scrollTop = window.scrollY;
        let windowTop = scrollTop + window.innerHeight;
        let els = document.querySelectorAll('.wowo:not(.animated)');
        for (const i of els) {
            const eh = i.getBoundingClientRect().top + window.scrollY;
            const oh = eh + i.innerHeight;
            if ((scrollTop <= eh && windowTop >= eh) || (scrollTop <= oh && oh <= windowTop)) {
                i.classList.add('animated');
            }
        }
    }
}
