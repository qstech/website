import React, {Component} from "react";
import './sidebar.scss';
import {Link} from "react-router-dom";
import accountCenter from '../assets/icons/account-center.svg';
import weChat from '../assets/icons/wechat.svg';
import whatsApp from '../assets/icons/whatsapp.svg';
import contact from '../assets/icons/contact.svg';
import email from '../assets/icons/email.svg';
import wechatQr from '../assets/wechat-qr.jpeg';

class Sidebar extends Component {
    render() {
        return (
            <div className={'sidebar'}>
                <ul>
                    {
                        this.props.userObject && (
                            <li>
                                <Link to={'/account-center'}>
                                    <img src={accountCenter} alt={''} />
                                </Link>
                                <div className={'tooltip'}>
                                    Account Center
                                </div>
                            </li>
                        )
                    }
                    <li>
                        <a>
                            <img src={weChat} alt={''} />
                        </a>
                        <div className={'hover'}>
                            <img src={wechatQr} alt={''}/>
                        </div>
                    </li>
                    <li>
                        <a href='https://wa.me/+61498999399' target={'_blank'} rel={'noreferrer'}>
                            <img src={whatsApp} style={{width: '100%'}} alt={''} />
                        </a>
                        <div className={'tooltip'}>
                            <p>
                                <strong>Whatsapp / Signal</strong>
                                <br />
                                +61 4 9399 9899
                            </p>
                        </div>
                    </li>
                    <li>
                        <a href='tel:+61 2 9438 1451' target={'_blank'} rel={'noreferrer'}>
                            <img src={contact} alt={''} />
                        </a>
                        <div className={'tooltip'}>
                            <p>
                                <strong>Office</strong>
                            </p>
                            <p>
                                <strong>Local</strong>
                                <br />
                                02 9438 1451
                            </p>
                            <p>
                                <strong>Overseas</strong>
                                <br />
                                +61 2 9438 1451
                            </p>
                            <p>
                                <strong>Mon – Fri</strong>
                                <br />
                                10:00 am – 5:00 pm
                            </p>
                        </div>
                    </li>
                    <li>
                        <a href='mailto:info@commonrealty.com.au' target={'_blank'} rel={'noreferrer'}>
                            <img src={email} alt={''} />
                        </a>
                        <div className={'tooltip'}>
                            Email Us
                        </div>
                    </li>
                </ul>
            </div>
        );
    }
}

export default Sidebar;
