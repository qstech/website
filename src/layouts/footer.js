import React, {Component} from "react";
import "./footer.scss";
import {Link} from "react-router-dom";
import footerBg from '../assets/footer-bg.svg';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFacebookF, faInstagram, faPinterestP, faTwitter} from "@fortawesome/free-brands-svg-icons";
import Modals from "./modals";
import LoginModal from "../components/modals/login-modal";
import AuthService from "../services/auth.service";

class Footer extends Component {
    handleLoginClick = (ev=false) => {
        ev.preventDefault();
        if (!this.props.userObject) {
            Modals.create(<LoginModal />);
        } else {
            AuthService.deleteToken();
            window.location.reload();
        }
    }

    render() {
        return (
            <footer>
                <div className={'top'} style={{background: '#00AE9E url(\'' + footerBg + '\') right bottom no-repeat'}}>
                    <div className={'container'}>
                        <ul className={'col'}>
                            <li><Link to={'/'}>Home</Link></li>
                            <li><Link to={'/projects'}>Projects</Link></li>
                            <li><Link to={'/media'}>Media</Link></li>
                            <li><Link to={'/about-us'}>About Us</Link></li>
                            <li><Link to={'/market-insight'}>Market Insights</Link></li>
                            <li><Link to={'/market-insight/?tag=expert-advice'}>Expert Advice</Link></li>
                            <li><Link to={'/market-insight/?tag=investor-buyers-guide'}>Investor & Buyers Guide</Link></li>
                        </ul>
                        <ul className={'col'}>
                            <li><Link to={'/services'}>Services</Link></li>
                            <li><Link to={'/services/#lease-management'}>Lease & Management</Link></li>
                            <li><Link to={'/services/#real-estate-advisory'}>Real Estate Advisory</Link></li>
                            <li><Link to={'/services/#project-marketing'}>Project Marketing</Link></li>
                            <li><Link to={'/services/#property-portfolio-management'}>Property Portfolio Management</Link></li>
                            <li><Link to={'/services/#property-sale'}>Property Sale</Link></li>
                            <li><Link to={'/services/#overseas-clients'}>Client from Overseas</Link></li>
                        </ul>
                        <ul className={'col'}>
                            <li><a onClick={this.handleLoginClick}>{this.props.userObject ? 'Logout' : 'Login / Register'}</a></li>
                            {/*<li><Link to={'/'}>My Favourite</Link></li>*/}
                            <li><Link to={'/contact-us'}>Contact Us</Link></li>
                        </ul>
                        <div className={'social'}>
                            <a rel={'noreferrer'} target={'_blank'} href={'/'}><FontAwesomeIcon icon={faFacebookF}/></a>
                            <a rel={'noreferrer'} target={'_blank'} href={'/'}><FontAwesomeIcon icon={faTwitter}/></a>
                            <a rel={'noreferrer'} target={'_blank'} href={'/'}><FontAwesomeIcon icon={faInstagram}/></a>
                            <a rel={'noreferrer'} target={'_blank'} href={'/'}><FontAwesomeIcon icon={faPinterestP}/></a>
                        </div>
                    </div>
                </div>
                <div className={'bottom'}>
                    <div className={'left'}>
                        <p>Copyright {new Date().getFullYear()} Common Realty Group</p>
                    </div>
                    <div className={'center'}>
                        <ul>
                            <li><Link to={'/terms-and-conditions/'}>Terms & Conditions</Link></li>
                            <li><Link to={'/privacy-policy/'}>Disclaimer</Link></li>
                        </ul>
                    </div>
                    <div className={'right'}>
                        <p>Website Designed & Powered by <a target='_blank' rel={'noreferrer'} href='https://nexty.com.au'>Nexty</a></p>
                    </div>
                </div>
            </footer>
        );
    }
}

export default Footer;
