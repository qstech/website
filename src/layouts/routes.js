const routes = [
    {
        exact: true,
        strict: true,
        animated: true,
        url: '/home',
        name: 'home',
        component: 'pages/home/home'
    },
    {
        strict: false,
        exact: true,
        animated: true,
        url: '/privacy-policy',
        name: 'privacy-policy',
        component: 'pages/privacy-policy/privacy-policy'
    },
    {
        strict: false,
        exact: true,
        animated: true,
        url: '/terms-and-conditions',
        name: 'terms-and-conditions',
        component: 'pages/t_c/t_c'
    },
    {
        exact: false,
        strict: false,
        animated: true,
        url: '/projects',
        name: 'projects',
        component: 'pages/projects/projects'
    },
    {
        exact: false,
        strict: false,
        animated: true,
        url: '/market-insight',
        name: 'market-insight',
        component: 'pages/market-insight/market-insight'
    },
    {
        exact: false,
        strict: false,
        animated: true,
        url: '/media',
        name: 'media',
        component: 'pages/media/media'
    },
    {
        strict: false,
        exact: true,
        animated: true,
        url: '/contact-us',
        name: 'contact-us',
        component: 'pages/contact-us/contact-us'
    },
    {
        strict: false,
        exact: true,
        animated: true,
        url: '/about-us',
        name: 'about-us',
        component: 'pages/about-us/about-us'
    },
    {
        strict: false,
        exact: true,
        animated: true,
        url: '/services',
        name: 'services',
        component: 'pages/services/services'
    },
    {
        strict: false,
        exact: true,
        animated: true,
        url: '/our-team',
        name: 'our-team',
        component: 'pages/our-team/our-team'
    },
    {
        strict: false,
        exact: true,
        animated: true,
        url: '/account-center',
        name: 'account-center',
        component: 'pages/account-center/account-center'
    }
]

export default routes;
