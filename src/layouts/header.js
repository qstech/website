import React, {Component} from "react";
import "./header.scss";
import {Link, NavLink} from "react-router-dom";
import siteLogo from '../assets/commonrealty-logo.svg';
import {withRouter} from "react-router";
import Modals from "./modals";
import LoginModal from "../components/modals/login-modal";
import AgentApplicationModal from "../components/modals/agent-application-modal";
import AuthService from "../services/auth.service";

class Header extends Component {

    constructor() {
        super();
        this.state = {
            mobileActive: false
        }
    }

    componentDidMount() {
        if (this.props.location.hash === '#login') {
            this.handleLoginClick();
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.location.hash === '#login') {
            this.handleLoginClick();
        }
    }

    handleLoginClick = (ev=false) => {
        ev.preventDefault();
        if (!this.props.userObject) {
            Modals.create(<LoginModal />);
        } else {
            AuthService.deleteToken();
            window.location.reload();
        }
    }

    handlePartnerClick = (ev) => {
        if (ev) {
            ev.preventDefault();
        }
        Modals.create(<AgentApplicationModal userObject={this.props.userObject} />);
    }

    render() {
        const toggleMobileMenu = () => {
            this.setState({mobileActive: !this.state.mobileActive})
        }

        return (
            <>
                <header>
                    <div className={'container'}>
                        <div className={'top'}>
                            <div>
                                <ul className={'menu'}>
                                    {
                                        !this.props.userObject || this.props.userObject.user_type !== 'Agent' ?
                                            <li><Link to="/" onClick={this.handlePartnerClick}>Partner</Link></li> :
                                            <li />
                                    }
                                    <li><NavLink to={'/our-team'} activeClassName={'router-active'}>Our Team</NavLink></li>
                                    <li><NavLink to={'/about-us'} activeClassName={'router-active'}>About us</NavLink></li>
                                    <li><NavLink to={'/contact-us'} activeClassName={'router-active'}>Contact Us</NavLink></li>
                                </ul>
                                <div className={'buttons'}>
                                    <div className={'switch-lang-wrap'}>
                                        <button className={'switch-lang'}>Switch</button>
                                    </div>
                                    <a className={'login'} href={'/account-center'} onClick={this.handleLoginClick}>
                                        { !this.props.userObject ? 'Login/Register' : 'Log out'}
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className={'bottom'}>
                            <div className={'logo'}>
                                <a href={'/'}>
                                    <img src={siteLogo} alt={'Commonrealty Logo'}/>
                                </a>
                            </div>
                            <ul className={'menu'}>
                                <li><NavLink to={'/home'} activeClassName={'router-active'}>Home</NavLink></li>
                                <li><NavLink to={'/projects'} activeClassName={'router-active'}>Projects</NavLink></li>
                                <li><NavLink to={'/services'} activeClassName={'router-active'}>Services</NavLink></li>
                                {/*<li><NavLink to={'/market-insight'} activeClassName={'router-active'}>Market Insight</NavLink></li>*/}
                                <li><NavLink to={'/media'} activeClassName={'router-active'}>Media</NavLink></li>
                            </ul>
                            <div className="nav-hamburger" onClick={toggleMobileMenu}>
                                <button className={`hamburger hamburger--slider ${this.state.mobileActive ? 'is-active': ''}`} type="button">
                                <span className="hamburger-box">
                                    <span className="hamburger-inner"/>
                                </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </header>
                <div className={`mobile-menu ${this.state.mobileActive ? 'is-active': ''}`}>
                    <div>
                        <ul>
                            <ul id="menu-mobile-menu" className="">
                                <li><NavLink onClick={toggleMobileMenu} to={'/home'} activeClassName={'router-active'}>Home</NavLink></li>
                                <li><NavLink onClick={toggleMobileMenu} to={'/projects'} activeClassName={'router-active'}>Projects</NavLink></li>
                                <li><NavLink onClick={toggleMobileMenu} to={'/services'} activeClassName={'router-active'}>Services</NavLink></li>
                                {/*<li><NavLink onClick={toggleMobileMenu} to={'/market-insight'} activeClassName={'router-active'}>Market Insight</NavLink></li>*/}
                                <li><NavLink onClick={toggleMobileMenu} to={'/media'} activeClassName={'router-active'}>Media</NavLink></li>
                                <li><NavLink onClick={toggleMobileMenu} to={'/our-team'} activeClassName={'router-active'}>Our Team</NavLink></li>
                                <li><NavLink onClick={toggleMobileMenu} to={'/about-us'} activeClassName={'router-active'}>About us</NavLink></li>
                                <li><NavLink onClick={toggleMobileMenu} to={'/contact-us'} activeClassName={'router-active'}>Contact Us</NavLink></li>
                            </ul>
                        </ul>
                        <div className='login'>
                            <a className={'btn'} href={'/account-center'} onClick={this.handleLoginClick}>
                                { !this.props.userObject ? 'Login/Register' : 'Log out'}
                            </a>
                            {/*{*/}
                            {/*    !this.props.userObject || this.props.userObject.user_type !== 'Agent' ?*/}
                            {/*        <Link className='btn agent' to="/" onClick={this.handlePartnerClick}>Partner</Link> : undefined*/}
                            {/*}*/}
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default withRouter(Header);
