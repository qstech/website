import axios from "axios";
import config from "../common/config";
import AuthService from "./auth.service";
import {message} from "antd";

// interface CustomAxiosOptions {
//     attachAuth?: boolean,
//     external?: boolean,
//     useFormData?: boolean
// }

export default function axiosService(options = {
    attachAuth: true,
    useFormData: false
}) {
    const axiosConfig = {
        baseURL: options?.external ? '' : config.backendURL
    };
    const http = axios.create(axiosConfig);
    http.interceptors.response.use(response => {
        return response;
    }, error => {
        if (error.response?.status === 401 && options.attachAuth) {
            const refreshToken = AuthService.getToken("refresh");
            if (refreshToken) {
                return AuthService.refreshToken(refreshToken).then(
                    response => {
                        if (response.status !== 200) {
                            message.warning({
                                content: 'Your login session has expired, please log in again.'
                            });
                            AuthService.deleteToken();
                            return error.response;
                        } else {
                            AuthService.saveToken({access: response.data.access});
                            const config = error.config;
                            config.headers.Authorization = `Bearer ${response.data.access}`;
                            return new Promise((resolve, reject) => {
                                http.request(config).then(
                                    res => resolve(res)
                                ).catch( error => reject(error));
                            });
                        }
                    }
                );
            }
        }
        return error.response;
    });

    http.interceptors.request.use((request) => {
        let accessToken = AuthService.getToken("access");
        if (options.attachAuth) {
            if (accessToken) {
                request.headers.Authorization = `Bearer ${accessToken}`;
            }
        }
        if (options.useFormData) {
            request.headers['Content-Type'] = 'multipart/form-data';
        }
        return request;
    });

    return http;
}
