import config from "../common/config";
import axiosService from "./axios.service";
import jwtDecode from "jwt-decode";

export default class AuthService {
    static login = (username, password) => {
        return axiosService().post(
            'users/token/',
            {
                username,
                password
            }
        );
    }

    static register = (values) => {
        return axiosService().post(
            'users/register/',
            values
        );
    }

    static checkTokenExpiry = (token) => {
        const token_expiry = jwtDecode(token);
        return token_expiry ? token_expiry['exp'] > (Date.now()/1000) : false;
    }

    static refreshToken = (refreshToken) => {
        return axiosService().post(
            'users/token/refresh/',
            {
                refresh: refreshToken
            }
        ).then(
            (res) => {
                if (res.status === 401) {
                    this.deleteToken();
                }
                return res;
            }
        );
    }

    static getCurrentUser = () => {
        return axiosService({attachAuth: true}).get(
            'users/'
        );
    }

    static saveToken = (tokenObject) => {
        if (tokenObject.access) {
            localStorage.setItem(config.tokenKey, tokenObject.access);
        }
        if (tokenObject.refresh) {
            localStorage.setItem(config.refreshTokenKey, tokenObject.refresh);
        }
    }

    static deleteToken = (tokenType=undefined) => {
        if (!tokenType) {
            localStorage.clear();
        } else if (tokenType === 'access') {
            localStorage.removeItem(config.tokenKey);
        } else if (tokenType === 'refresh') {
            localStorage.removeItem(config.refreshTokenKey);
        }
    }

    static getToken = (tokenType) => {
        const accessToken = localStorage.getItem(config.tokenKey);
        const refreshToken = localStorage.getItem(config.refreshTokenKey);
        if (tokenType === 'access' && accessToken) {
            if (!this.checkTokenExpiry(accessToken)) {
                this.deleteToken('access');
            }
            return localStorage.getItem(config.tokenKey) || null;
        }
        if (tokenType === 'refresh' && !!refreshToken) {
            if (!this.checkTokenExpiry(refreshToken)) {
                this.deleteToken('refresh');
            }
            return localStorage.getItem(config.refreshTokenKey) || null;
        }
        if (tokenType === 'all' && accessToken && refreshToken) {
            if (!this.checkTokenExpiry(accessToken)) {
                this.deleteToken('access');
            }
            if (!this.checkTokenExpiry(refreshToken)) {
                this.deleteToken('refresh');
            }
            return {
                access: localStorage.getItem(config.tokenKey) || null,
                refresh: localStorage.getItem(config.refreshTokenKey) || null
            }
        }
    }
}
